﻿using TMPro;
using UnityEngine;

public class OnScreenMessage : MonoBehaviour
{
    [SerializeField] private TMP_Text text;

    [SerializeField] private float textLifeTime;

    private float _timeDisplayed;

    public bool _firstTimeTextIsEmpty = true;

    private void Update()
    {
        if (text.text != "")
        {
            if (_firstTimeTextIsEmpty)
            {
                _timeDisplayed = Time.deltaTime;
                _firstTimeTextIsEmpty = false;
            }
            else
            {
                _timeDisplayed += Time.deltaTime;

                if (_timeDisplayed >= textLifeTime)
                {
                    text.text = "";
                    text.color = Color.white;
                    _timeDisplayed = 0;
                    _firstTimeTextIsEmpty = true;
                }
            }
        }
    }
}
