﻿using System.Collections.Generic;
using Pandora.Components;
using Pandora.Data;
using Pandora.MonoBehaviours;
using Unity.Entities;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Jobs;

namespace Pandora.Systems
{
    public class VillagerResourceSearchSystem : ComponentSystem
    {
        public struct Villagers
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentArray<Villager> _villagers;
            public ComponentArray<ResourceStorage> _storage;
            public ComponentArray<NavMeshAgent> _navAgent;
            public ComponentDataArray<Harvesting> _harvesting;
            public TransformAccessArray _transforms;
        }

        public struct Resources
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentDataArray<ResourceSource> _resourceSource;
            public ComponentArray<ResourceStorage> _storage;
            public TransformAccessArray _transforms;
            public SubtractiveComponent<ResourceExhausted> _exhaustedTag;
        }

        [Inject] private Villagers _villagers;
        [Inject] private Resources _resources;
        
        protected override void OnUpdate()
        {
            Dictionary<Entity, BeingHarvested> newBeingHarvesteds = new Dictionary<Entity, BeingHarvested>();

            for (int villagerIndex = 0; villagerIndex < _villagers.Length; villagerIndex++)
            {
                Harvesting harvestingStatus = _villagers._harvesting[villagerIndex];

                if (harvestingStatus._state == HarvestingState.SEARCHING)
                {
                    Transform villagerTransform = _villagers._transforms[villagerIndex];
                    NavMeshAgent navMesh = _villagers._navAgent[villagerIndex];

                    if (_resources.Length > 0)
                    {
                        int closestResource = 0;
                        float closestResourceDistance = -1.0f;

                        for (int resourceIndex = 0; resourceIndex < _resources.Length; resourceIndex++)
                        {
                            if (_resources._storage[resourceIndex]._type == _villagers._storage[villagerIndex]._type)
                            {
                                Transform resourceTransform = _resources._transforms[resourceIndex];

                                float distance = (resourceTransform.position - harvestingStatus._position).magnitude;

                                if (distance < closestResourceDistance || closestResourceDistance < 0)
                                {
                                    closestResource = resourceIndex;
                                    closestResourceDistance = distance;
                                }
                            }
                        }

                        harvestingStatus._targetEntity = _resources._entities[closestResource];
                        harvestingStatus._state = HarvestingState.HARVESTING;
                        harvestingStatus._position = _resources._transforms[closestResource].position;

                        
                        if (EntityManager.HasComponent<BeingHarvested>(_resources._entities[closestResource]))
                        {
                            BeingHarvested harvestedElement = EntityManager.GetComponentData<BeingHarvested>(_resources._entities[closestResource]);
                            harvestedElement._harvesterCount++;
                            PostUpdateCommands.SetComponent(_resources._entities[closestResource], harvestedElement);
                        }
                        else
                        {
                            if (newBeingHarvesteds.ContainsKey(_resources._entities[closestResource]))
                            {
                                BeingHarvested newBeingHarvested = newBeingHarvesteds[_resources._entities[closestResource]];
                                newBeingHarvested._harvesterCount++;
                                newBeingHarvesteds[_resources._entities[closestResource]] = newBeingHarvested;
                            }
                            else
                            {
                                newBeingHarvesteds.Add(_resources._entities[closestResource], new BeingHarvested{_harvesterCount = 1});
                            }
                        }
                        
                        navMesh.SetDestination(_resources._transforms[closestResource].position);
                        
                        PostUpdateCommands.SetComponent(_villagers._entities[villagerIndex], harvestingStatus);
                    }
                }
            }
            
            foreach (KeyValuePair<Entity,BeingHarvested> newBeingHarvested in newBeingHarvesteds)
            {
                PostUpdateCommands.AddComponent(newBeingHarvested.Key, newBeingHarvested.Value);
            }
        }
    }
}