using Unity.Entities;

namespace Pandora.Systems
{
    public class GameManagerSystem : ComponentSystem
    {
        public struct GameManagers
        {
            public readonly int Length;
            public ComponentArray<GameManager> _gameManagers;
        }

        [Inject] private GameManagers _gameManagers;
        
        protected override void OnUpdate()
        {
            GameManager gameManager = _gameManagers._gameManagers[0];
        }
    }
}
