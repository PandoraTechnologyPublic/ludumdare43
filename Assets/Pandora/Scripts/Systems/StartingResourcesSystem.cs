using Pandora.Components;
using Pandora.Data;
using Pandora.MonoBehaviours;
using Unity.Entities;
using UnityEngine;

namespace Pandora.Systems
{
    public class StartingResourcesSystem : ComponentSystem
    {        
        public struct StartingResources
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentArray<ResourceStorage> _resourceStorages;
            public ComponentArray<StartingResource> _startingResource;
            public SubtractiveComponent<Used> _useds;
        }

        [Inject] private StartingResources _startingResources;
        
        public struct ResourceStates
        {
            public readonly int Length;
            public ComponentDataArray<ResourceState> _resourceStates;
        }

        [Inject] private ResourceStates _resourceStates;
        
        protected override void OnUpdate()
        {
            if (_resourceStates.Length > 0)
            {
                ResourceState resourceState = _resourceStates._resourceStates[0];
                
                for (int startingResourceIndex = 0; startingResourceIndex < _startingResources._resourceStorages.Length; startingResourceIndex++)
                {
                    ResourceStorage startingResource = _startingResources._resourceStorages[startingResourceIndex];

                    switch (startingResource._type)
                    {
                        case ResourceType.WOOD:
                        {
                            resourceState._woodQuantityCurrent += startingResource._storedValue;
                            break;
                        }
                        case ResourceType.STONE:
                        {
                            resourceState._stoneQuantityCurrent += startingResource._storedValue;
                            break;
                        }
                        case ResourceType.FOOD:
                        {
                            resourceState._foodQuantityCurrent += startingResource._storedValue;
                            break;
                        }
                    }
                    
                    PostUpdateCommands.AddComponent(_startingResources._entities[startingResourceIndex], new Used());
                }

                _resourceStates._resourceStates[0] = resourceState;
            }
        }
    }
}
