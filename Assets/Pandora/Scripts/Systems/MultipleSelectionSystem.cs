using Pandora.Components;
using Pandora.Data;
using Unity.Entities;
using UnityEngine;

namespace Pandora.Systems
{
    public class MultipleSelectionSystem : ComponentSystem
    {
        public struct Villagers
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentArray<Villager> _villagers;
            public ComponentArray<Transform> _transforms;
        }

        [Inject] private Villagers _villagers;
        
        public struct GameManagers
        {
            public readonly int Length;
            public ComponentArray<GameManager> _gameManagers;
        }

        [Inject] private GameManagers _gameManagers;
        public struct IngameStates
        {
            public readonly int Length;
            public ComponentDataArray<GameState> _gameStates;
            public ComponentDataArray<IngameState> _ingameStates;
        }

        [Inject] private IngameStates _ingameStates;
        
        private bool _isDragging;
        private Vector3 _initialHitInfoPointPosition;
        private int _layoutMask;
        private GameObject _selectionBox;

        protected override void OnStartRunning()
        {
            _layoutMask = LayerMask.GetMask("Ground");
        }

        protected override void OnUpdate()
        {
            if (_ingameStates.Length > 0)
            {
                if (Input.GetMouseButtonDown(0))
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hitInfo;

                    if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, _layoutMask))
                    {
                        _isDragging = true;
                        _initialHitInfoPointPosition = hitInfo.point;
//                        Debug.Log("click : " + _initialHitInfoPointPosition);

                        if (_selectionBox == null)
                        {
                            _selectionBox = GameObject.Instantiate(_gameManagers._gameManagers[0]._multipleSelectionPrefab);
                        }
                    }
                }
                else if (_isDragging && !Input.GetMouseButtonUp(0))
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hitInfo;

                    if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity,  _layoutMask))
                    {
                        Vector3 hitInfoPoint = hitInfo.point;
                        Vector3 center = (hitInfoPoint + _initialHitInfoPointPosition) / 2.0f;
                        Vector3 size = hitInfoPoint - _initialHitInfoPointPosition;
                    
                        size.x = Mathf.Abs(size.x);
                        size.y = 2.5f;
                        size.z = Mathf.Abs(size.z);

                        _selectionBox.transform.position = center;
                        _selectionBox.transform.localScale = size;
                    
                        _selectionBox.gameObject.SetActive(true);
                    }
                }
                else if (_isDragging && Input.GetMouseButtonUp(0))
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hitInfo;
                
                    if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, _layoutMask))
                    {
                        Vector3 hitInfoPoint = hitInfo.point;
                        Vector3 center = (hitInfoPoint + _initialHitInfoPointPosition) / 2.0f;
                        Vector3 size = hitInfoPoint - _initialHitInfoPointPosition;
                        size.x = Mathf.Abs(size.x);
                        size.y = 2.5f;
                        size.z = Mathf.Abs(size.z);
                        Bounds bounds = new Bounds(center, size);
                    
//                        Debug.Log("release : " + hitInfoPoint);

                        for (int villagerIndex = 0; villagerIndex < _villagers.Length; villagerIndex++)
                        {
                            Transform villagersTransform = _villagers._transforms[villagerIndex];

                            if (bounds.Contains(villagersTransform.position))
                            {
                                PostUpdateCommands.AddComponent(_villagers._entities[villagerIndex], new Clicked());
                            }
                        }
                    }
                    _isDragging = false;
                    _selectionBox.gameObject.SetActive(false);
                }
            }
        }
    }
}
