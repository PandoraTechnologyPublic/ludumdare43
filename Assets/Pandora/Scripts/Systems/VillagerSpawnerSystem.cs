﻿using System.Collections.Generic;
using Pandora.Components;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Jobs;
using Pandora.Prefabs;
using Pandora.Data;
using Random = UnityEngine.Random;

namespace Pandora.Systems
{
    public class VillagerSpawnerSystem : ComponentSystem
    {
        public struct VillagerSpawners
        {
            public readonly int Length;
            public ComponentArray<Spawner> _spawners;
            public ComponentArray<VillagerPrefab> _villagers;
            public ComponentArray<TrainingCost> _trainingCosts;
            public TransformAccessArray _transforms;
        }

        [Inject] private VillagerSpawners _spawners;

        public struct GameStates
        {
            public readonly int Length;
            public ComponentDataArray<GameState> _gameStates;
        }

        [Inject] private GameStates _gameStates;
        
        public struct ResourceStates
        {
            public readonly int Length;
            public ComponentDataArray<ResourceState> _resourceStates;
        }

        [Inject] private ResourceStates _resourceStates;
        
        protected override void OnUpdate()
        {
            ResourceState resourceState = _resourceStates._resourceStates[0];

            if (resourceState._populationQuantityCurrent < resourceState._populationQuantityMax)
            {
                List<Transform> triggeredSpawnersTransforms = new List<Transform>(_spawners.Length);
                List<VillagerPrefab> triggeredSpawnersPrefabs = new List<VillagerPrefab>(_spawners.Length);

                for (int spawnerIndex = 0; spawnerIndex < _spawners.Length; spawnerIndex++)
                {
                    Spawner spawner = _spawners._spawners[spawnerIndex];
                    int foodCost = _spawners._trainingCosts[spawnerIndex]._foodCost;
                    
                    if (foodCost <= resourceState._foodQuantityCurrent)
                    {
                        spawner._state = Spawner.SpawnState.SPAWNING;
                        spawner._deltaTime += Time.deltaTime;

                        if (spawner._deltaTime >= spawner._triggerTime)
                        {
                            triggeredSpawnersTransforms.Add(_spawners._transforms[spawnerIndex]);
                            triggeredSpawnersPrefabs.Add(_spawners._villagers[spawnerIndex]);

                            spawner._deltaTime = 0;
                            resourceState._foodQuantityCurrent -= foodCost;
                        }
                    }
                    else
                    {
                        spawner._state = Spawner.SpawnState.NOT_ENOUGH_FOOD;
                    }
                }

                _resourceStates._resourceStates[0] = resourceState;

                // The following will invalidate native arrays; must come last in execution flow
                int creationCount = triggeredSpawnersTransforms.Count;
                Vector3 defaultRotation = new Vector3(0, Random.Range(0, 360), 0);
                
                for (int i = 0; i < creationCount; i++)
                {
                    Villager gameObject = GameObject.Instantiate(triggeredSpawnersPrefabs[i]._prefab, triggeredSpawnersTransforms[i]);
                    gameObject.transform.Rotate(defaultRotation, Space.Self);
                }
            }
            else
            {
                for (int i = 0; i < _spawners.Length; i++)
                {
                    _spawners._spawners[i]._state = Spawner.SpawnState.CAP_REACHED;
                }
            }
        }
    }
}
