using Pandora.Data;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Pandora.Systems.States
{
    public class GameOverStateSystem : ComponentSystem
    {
        public struct GameStates
        {
            public readonly int Length;
            [ReadOnly] public EntityArray _entities;
            [ReadOnly] public ComponentDataArray<GameState> _gameStates;
            [ReadOnly] public ComponentDataArray<GameOverState> _gameOverStates;
        }
        
        [Inject] private GameStates _gameStates;
        
        public struct AnyKeyGameStates
        {
            public readonly int Length;
            [ReadOnly] public EntityArray _entities;
            [ReadOnly] public ComponentDataArray<GameState> _gameStates;
            [ReadOnly] public ComponentDataArray<GameOverState> _welcomeStates;
            [ReadOnly] public ComponentDataArray<OnAnyKeyUp> _onAnyKeyUps;
        }
        
        [Inject] private AnyKeyGameStates _anyKeyGameStates;

        public struct MainUiLayouts
        {
            public readonly int Length;
            public ComponentArray<MainUiLayout> _mainUiLayouts;
        }
        
        [Inject] private MainUiLayouts _mainUiLayouts;

        public struct Cameras
        {
            public readonly int Length;
            [ReadOnly] public ComponentArray<Camera> _cameras;
        }
        
        [Inject] private Cameras _cameras;
        
        protected override void OnUpdate()
        {
            if (_gameStates.Length > 0)
            {
                _mainUiLayouts._mainUiLayouts[0].SelectGameOverScreen();
            }
            
            if (_anyKeyGameStates.Length > 0)
            {
                Entity gameStatesEntity = _anyKeyGameStates._entities[0];
                
                PostUpdateCommands.RemoveComponent<OnAnyKeyUp>(gameStatesEntity);
                PostUpdateCommands.RemoveComponent<GameOverState>(gameStatesEntity);
                PostUpdateCommands.AddComponent(gameStatesEntity, new MainMenuState());

                SceneManager.UnloadSceneAsync("Map001");
            
//                _cameras._cameras[0].gameObject.SetActive(true);
            }
        }
    }
}
