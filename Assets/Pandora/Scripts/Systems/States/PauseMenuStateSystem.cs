using Pandora.Data;
using Unity.Entities;
using UnityEngine;
using Unity.Collections;

namespace Pandora.Systems.States
{
    public class PauseMenuStateSystem : ComponentSystem
    {
        public struct GameStates
        {
            public readonly int Length;
            [ReadOnly] public EntityArray _entities;
            [ReadOnly] public ComponentDataArray<GameState> _gameStates;
            [ReadOnly] public ComponentDataArray<PauseMenuState> _pauseMenuStates;
            [ReadOnly] public SubtractiveComponent<OnEscapeKeyUp> _onEscapeKeyUp;
        }
        
        [Inject] private GameStates _gameStates;
        
        public struct OnEscapeKeyGameStates
        {
            public readonly int Length;
            [ReadOnly] public EntityArray _entities;
            [ReadOnly] public ComponentDataArray<GameState> _gameStates;
            [ReadOnly] public ComponentDataArray<PauseMenuState> _ingameStates;
            [ReadOnly] public ComponentDataArray<OnEscapeKeyUp> _onEscapeKeyUp;
        }
        
        [Inject] private OnEscapeKeyGameStates _onEscapeKeyGameStates;

        public struct MainUiLayouts
        {
            public readonly int Length;
            public ComponentArray<MainUiLayout> _mainUiLayouts;
        }
        
        [Inject] private MainUiLayouts _mainUiLayouts;

        protected override void OnUpdate()
        {
            if (_gameStates.Length > 0)
            {
                _mainUiLayouts._mainUiLayouts[0].SelectPauseMenuScreen();
            }
            
            if (_onEscapeKeyGameStates.Length > 0)
            {
                Entity gameStatesEntity = _onEscapeKeyGameStates._entities[0];
                
                PostUpdateCommands.RemoveComponent<OnEscapeKeyUp>(gameStatesEntity);
                PostUpdateCommands.RemoveComponent<PauseMenuState>(gameStatesEntity);
                PostUpdateCommands.AddComponent(gameStatesEntity, new IngameState());
            }
        }
    }
}
