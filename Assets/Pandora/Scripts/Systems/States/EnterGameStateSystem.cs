using Pandora.Data;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Pandora.Systems.States
{
    public class EnterGameStateSystem : ComponentSystem
    {
        public struct GameStates
        {
            public readonly int Length;
            [ReadOnly] public EntityArray _entities;
            [ReadOnly] public ComponentDataArray<GameState> _gameStates;
            [ReadOnly] public ComponentDataArray<EnterGameState> _enterGameStates;
        }
        
        [Inject] private GameStates _gameStates;

        public struct Cameras
        {
            public readonly int Length;
            [ReadOnly] public ComponentArray<Camera> _cameras;
        }
        
        [Inject] private Cameras _cameras;
        
        public struct MapGenerators
        {
            public readonly int Length;
            [ReadOnly] public ComponentArray<MapGenerator> _mapGenerates;
        }
        
        [Inject] private MapGenerators _mapGenerators;

        private bool _sceneFullLoaded = false;
        private AsyncOperation _asyncLoad = null;
        private string _mapSceneName;

        protected override void OnUpdate()
        {
            if (_gameStates.Length > 0) // meaning we are in EnterGameState
            {
                if ((_asyncLoad == null) && !_sceneFullLoaded)
                {
                    _mapSceneName = "Map001";
                    _asyncLoad = SceneManager.LoadSceneAsync(_mapSceneName, LoadSceneMode.Additive);
//                    _cameras._cameras[0].gameObject.SetActive(false);
                }
                else if (_asyncLoad.isDone)
                {
                    _sceneFullLoaded = true;
                }
                
                if (_mapGenerators.Length > 0)
                {
                    PostUpdateCommands.RemoveComponent<EnterGameState>(_gameStates._entities[0]);
                    PostUpdateCommands.AddComponent(_gameStates._entities[0], new IngameState());
                    SceneManager.SetActiveScene(SceneManager.GetSceneByName(_mapSceneName));
                    _mapGenerators._mapGenerates[0].GenerateMap();
                    _sceneFullLoaded = false;
                    _asyncLoad = null;
                }
            }
        }
    }
}
