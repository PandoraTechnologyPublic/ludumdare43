using Pandora.Data;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace Pandora.Systems.States
{
    public class WelcomeStateSystem : ComponentSystem
    {
        public struct GameStates
        {
            public readonly int Length;
            [ReadOnly] public EntityArray _entities;
            [ReadOnly] public ComponentDataArray<GameState> _gameStates;
            [ReadOnly] public ComponentDataArray<WelcomeState> _welcomeStates;
        }
        
        [Inject] private GameStates _gameStates;
        
        public struct AnyKeyGameStates
        {
            public readonly int Length;
            [ReadOnly] public EntityArray _entities;
            [ReadOnly] public ComponentDataArray<GameState> _gameStates;
            [ReadOnly] public ComponentDataArray<WelcomeState> _welcomeStates;
            [ReadOnly] public ComponentDataArray<OnAnyKeyUp> _onAnyKeyUps;
        }
        
        [Inject] private AnyKeyGameStates _anyKeyGameStates;

        public struct MainUiLayouts
        {
            public readonly int Length;
            public ComponentArray<MainUiLayout> _mainUiLayouts;
        }
        
        [Inject] private MainUiLayouts _mainUiLayouts;
        
        protected override void OnUpdate()
        {
            if (_gameStates.Length > 0)
            {
                _mainUiLayouts._mainUiLayouts[0].SelectWelcomeScreen();
            }
            
            if (_anyKeyGameStates.Length > 0)
            {
                Entity gameStatesEntity = _anyKeyGameStates._entities[0];
                
                PostUpdateCommands.RemoveComponent<WelcomeState>(gameStatesEntity);
                PostUpdateCommands.RemoveComponent<OnAnyKeyUp>(gameStatesEntity);
                PostUpdateCommands.AddComponent(gameStatesEntity, new MainMenuState());
            }
        }
    }
}
