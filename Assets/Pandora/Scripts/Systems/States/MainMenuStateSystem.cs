using Pandora.Data;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace Pandora.Systems.States
{
    public class MainMenuStateSystem : ComponentSystem
    {
        public struct GameStates
        {
            public readonly int Length;
            [ReadOnly] public EntityArray _entities;
            [ReadOnly] public ComponentDataArray<GameState> _gameStates;
            [ReadOnly] public ComponentDataArray<MainMenuState> _mainMenuStates;
        }
        
        [Inject] private GameStates _gameStates;

        public struct MainUiLayouts
        {
            public readonly int Length;
            [ReadOnly] public ComponentArray<MainUiLayout> _mainUiLayouts;
        }
        
        [Inject] private MainUiLayouts _mainUiLayouts;

        public struct MainMenuLayouts
        {
            public readonly int Length;
            [ReadOnly] public EntityArray _entities;
            [ReadOnly] public ComponentArray<MainMenuLayout> _mainMenuLayouts;
            [ReadOnly] public ComponentDataArray<OnPlayButtonClicked> _onPlayButtonClickeds;
        }
        
        [Inject] private MainMenuLayouts _mainMenuLayouts;
        
        protected override void OnUpdate()
        {
            if (_gameStates.Length > 0)
            {
                _mainUiLayouts._mainUiLayouts[0].SelectMainMenuScreen();
            }

            if (_mainMenuLayouts.Length > 0)
            {
                PostUpdateCommands.RemoveComponent<OnPlayButtonClicked>(_mainMenuLayouts._entities[0]);
                
                Entity gameStatesEntity = _gameStates._entities[0];

                PostUpdateCommands.RemoveComponent<MainMenuState>(gameStatesEntity);
                PostUpdateCommands.AddComponent(gameStatesEntity, new EnterGameState());
            }
        }
    }
}
