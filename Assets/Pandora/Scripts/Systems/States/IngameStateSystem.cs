using Pandora.Data;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace Pandora.Systems.States
{
    public class IngameStateSystem : ComponentSystem
    {
        public struct GameStates
        {
            public readonly int Length;
            [ReadOnly] public EntityArray _entities;
            [ReadOnly] public ComponentDataArray<GameState> _gameStates;
            [ReadOnly] public ComponentDataArray<IngameState> _ingameStates;
            [ReadOnly] public SubtractiveComponent<OnEscapeKeyUp> _onEscapeKeyUp;
        }
        
        [Inject] private GameStates _gameStates;
        
        public struct OnEscapeKeyGameStates
        {
            public readonly int Length;
            [ReadOnly] public EntityArray _entities;
            [ReadOnly] public ComponentDataArray<GameState> _gameStates;
            [ReadOnly] public ComponentDataArray<IngameState> _ingameStates;
            [ReadOnly] public ComponentDataArray<OnEscapeKeyUp> _onEscapeKeyUp;
        }
        
        [Inject] private OnEscapeKeyGameStates _onEscapeKeyGameStates;

        public struct MainUiLayouts
        {
            public readonly int Length;
            public ComponentArray<MainUiLayout> _mainUiLayouts;
        }
        
        [Inject] private MainUiLayouts _mainUiLayouts;

        protected override void OnUpdate()
        {
            if (_gameStates.Length > 0)
            {
                _mainUiLayouts._mainUiLayouts[0].SelectIngameScreen();
            }
            
//            if (_onEscapeKeyGameStates.Length > 0)
//            {
//                Entity gameStatesEntity = _onEscapeKeyGameStates._entities[0];
//
//                PostUpdateCommands.RemoveComponent<OnEscapeKeyUp>(gameStatesEntity);
//                PostUpdateCommands.RemoveComponent<IngameState>(gameStatesEntity);
//                PostUpdateCommands.AddComponent(gameStatesEntity, new PauseMenuState());
//            }
        }
    }
}
