using Pandora.Data;
using Unity.Collections;
using Unity.Entities;
using UnityEngine.SceneManagement;

namespace Pandora.Systems.States
{
    public class ExitGameStateSystem : ComponentSystem
    {
        public struct GameStates
        {
            public readonly int Length;
            [ReadOnly] public EntityArray _entities;
            [ReadOnly] public ComponentDataArray<GameState> _gameStates;
            [ReadOnly] public ComponentDataArray<ExitGameState> _exitGameStates;
        }
        
        [Inject] private GameStates _gameStates;

        protected override void OnUpdate()
        {
            PostUpdateCommands.RemoveComponent<ExitGameState>(_gameStates._entities[0]);
            PostUpdateCommands.AddComponent(_gameStates._entities[0], new GameOverState());
            
            //SceneManager.UnloadSceneAsync("Map001");
        }
    }
}
