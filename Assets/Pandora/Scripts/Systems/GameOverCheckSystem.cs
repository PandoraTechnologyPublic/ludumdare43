using Pandora.Data;
using Pandora.Systems.States;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace Pandora.Systems
{
    [UpdateAfter(typeof(PopulationHostingSystem))]
    public class GameOverCheckSystem : ComponentSystem
    {
        public struct GameStates
        {
            public readonly int Length;
            [ReadOnly] public EntityArray _entities;
            [ReadOnly] public ComponentDataArray<GameState> _gameStates;
            [ReadOnly] public ComponentDataArray<IngameState> _ingameStates;
        }

        public struct ResourceStates
        {
            public readonly int Length;
            public ComponentDataArray<ResourceState> _resourceStates;
        }

        [Inject] private GameStates _gameStates;
        [Inject] private ResourceStates _resourceStates;

        protected override void OnUpdate()
        {
            // Only one ResourceState is foreseen
            ResourceState resourceState = _resourceStates._resourceStates[0];

            if (_gameStates.Length > 0) // we are "IngameState"
            {
                if (((resourceState._populationQuantityCurrent <= 0) || (_gameStates._gameStates[0]._evilSatisfaction <= 0)))
                {
                    Entity gameStateEntity = _gameStates._entities[0];
                    PostUpdateCommands.RemoveComponent<IngameState>(gameStateEntity);
                    PostUpdateCommands.AddComponent(gameStateEntity, new ExitGameState());
                }
            }
        }
    }
}
