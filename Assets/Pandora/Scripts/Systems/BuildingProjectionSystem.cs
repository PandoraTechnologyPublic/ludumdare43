using Pandora.Components;
using Pandora.Data;
using Pandora.MonoBehaviours;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AI;

namespace Pandora.Systems
{
    public class BuildingProjectionSystem : ComponentSystem
    {    
        public struct SelectedBuildings
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentDataArray<SelectedBuilding> _selectedBuildings;
            public ComponentDataArray<BuildingId> _buildings;
        }

        [Inject] private SelectedBuildings _selectedBuildings;
        
        public struct CameraGroup
        {
            public readonly int Length;
            public ComponentArray<Camera> _cameras;
        }
    
        [Inject] private CameraGroup _cameras;
        
        public struct GameManagers
        {
            public readonly int Length;
            public ComponentArray<GameManager> _gameManagers;
        }

        [Inject] private GameManagers _gameManagers;

        public struct ResourceStates
        {
            public readonly int Length;
            [ReadOnly] public ComponentDataArray<ResourceState> _resourceStates;
        }
    
        [Inject] private ResourceStates _resourceStates;
        
        
        private int _groundLayerMask;
        private int _defaultLayerMask;
        
        protected override void OnStartRunning()
        {
            base.OnStartRunning();
            _groundLayerMask = LayerMask.GetMask("Ground");
            _defaultLayerMask = LayerMask.GetMask("Default");
        }

        protected override void OnUpdate()
        {
            if (_selectedBuildings.Length > 0)
            {
                // Compute & set hologram position
                Camera camera = _cameras._cameras[0];
                Ray mouseRay = camera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                Entity selectedBuildingsEntity = _selectedBuildings._entities[0];
                
                if (Physics.Raycast(mouseRay, out hit, 50, _groundLayerMask))
                {
                    Vector3 snappedPosition = hit.point;
                    
                    snappedPosition.x = Mathf.RoundToInt(snappedPosition.x);
                    snappedPosition.y = Mathf.RoundToInt(snappedPosition.y);
                    snappedPosition.z = Mathf.RoundToInt(snappedPosition.z);

                    float3 float3 = new float3(snappedPosition.x, snappedPosition.y, snappedPosition.z);
                    
                    PostUpdateCommands.SetComponent(selectedBuildingsEntity, new Position{Value = float3});
                    
                    // Check if building is allowed at that snappedPosition

                    GameManager gameManager = _gameManagers._gameManagers[0];
                    Building selectedBuildingPrefab = gameManager.GetBuildingPrefabById(_selectedBuildings._buildings[0]._id);
                    
                    NavMeshObstacle currentSelectedBuildingObstacle = selectedBuildingPrefab.GetComponentInChildren<NavMeshObstacle>();

                    BuildingCost buildingCost = selectedBuildingPrefab.GetComponent<BuildingCost>();

                    if (!Physics.CheckBox(float3, currentSelectedBuildingObstacle.size / 2.0f, default(Quaternion), _defaultLayerMask)
                    && ((_resourceStates._resourceStates[0]._stoneQuantityCurrent >= buildingCost._stoneCost)
                        && (_resourceStates._resourceStates[0]._woodQuantityCurrent >= buildingCost._woodCost)))
                    {
                        PostUpdateCommands.SetComponent(selectedBuildingsEntity, new SelectedBuilding{_isAllowed = 1});
                    }
                    else
                    {
                        PostUpdateCommands.SetComponent(selectedBuildingsEntity, new SelectedBuilding{_isAllowed = 0});   
                    }
                }
            }
        }
    }
}
