﻿using Pandora.Components;
using Pandora.Data;
using Pandora.UI;
using Unity.Entities;
using UnityEngine;

namespace Pandora.Systems
{
    public class AltarQuestHandlerSystem : ComponentSystem
    {
        public struct Altars
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentArray<Altar> _altars;
            public ComponentDataArray<AltarQuest> _quests;
            public ComponentArray<AltarUIBinder> _binders;
        }

        public struct Villagers
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentArray<Villager> _villagers;
            public ComponentDataArray<Interacting> _interacting;
        }
        
        public struct IngameStates
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentDataArray<GameState> _gameStates;
            public ComponentDataArray<IngameState> _ingameStates;
        }

        [Inject] private Altars _altars;
        [Inject] private IngameStates _ingameStates;
        [Inject] private Villagers _villagers;
        
        protected override void OnUpdate()
        {
            if (_ingameStates.Length == 0)
                return;
            
            GameState gameState = _ingameStates._gameStates[0];
            for (int altarIndex = 0; altarIndex < _altars.Length; altarIndex++)
            {
                Altar altar = _altars._altars[altarIndex];
                Entity altarEntity = _altars._entities[altarIndex];
                AltarQuest quest = _altars._quests[altarIndex];
                Entity entity = _altars._entities[altarIndex];
                AltarInfoMenu menu = _altars._binders[altarIndex]._menu;

                quest._remainingTime -= Time.deltaTime;
                
                if (quest._remainingTime <= 0)
                {
                    gameState._evilSatisfaction -= gameState._evilDrainRate;
                    PostUpdateCommands.RemoveComponent<AltarQuest>(entity);
                    PostUpdateCommands.SetComponent(_ingameStates._entities[0], gameState);
                    altar._display.gameObject.SetActive(false);
                    menu.ToggleSacrificeOrder(false);
                    
                    for (int villagerIndex = 0; villagerIndex < _villagers.Length; villagerIndex++)
                    {
                        if (_villagers._interacting[villagerIndex]._targetEntity == altarEntity)
                        {
                            PostUpdateCommands.RemoveComponent<Interacting>(_villagers._entities[villagerIndex]);
                        }
                    }

                    altar.textOnScreen.gameObject.SetActive(true);
                    string[] texts = new string[] {"Beware of the wrath of the Gods!", "You will regret the times you made fun of the Gods!", "You failed, mortals! Death is approching.","Where is our tribute?!"};
                    int randomText = Random.Range(0, texts.Length);
                    altar.textOnScreen.text = texts[randomText];
                    altar.textOnScreen.color = new Color32(0xE7, 0x15, 0x15, 0xFF);

                    altar.altarAudioSource.clip = altar.sacrificeFailed;
                    altar.altarAudioSource.Play();
                    altar.alwaysThunder.Play();

                    altar.music.PlayDelayed(20);
                }
                
                else if (IsQuestSucceed(quest))
                {
                    PostUpdateCommands.RemoveComponent<AltarQuest>(entity);
                    altar._display.gameObject.SetActive(false);
                    menu.ToggleSacrificeOrder(false);
                    
                    for (int villagerIndex = 0; villagerIndex < _villagers.Length; villagerIndex++)
                    {
                        if (_villagers._interacting[villagerIndex]._targetEntity == altarEntity)
                        {
                            PostUpdateCommands.RemoveComponent<Interacting>(_villagers._entities[villagerIndex]);
                        }
                    }

                    altar.textOnScreen.gameObject.SetActive(true);
                    altar.textOnScreen.color = Color.white;

                    string[] texts = new string[]{ "Your village remains safe. For Now.","A worthy sacrifice.", "Gods are pleased by your blind devotion.", "You won't regret it.", "Mmmh... Tasty.","You are still alive. It's a miracle."};

                    int randomText = Random.Range(0, texts.Length);
                    altar.textOnScreen.text = texts[randomText];
                    altar.textOnScreen.GetComponent<OnScreenMessage>()._firstTimeTextIsEmpty = true;

                    altar.altarAudioSource.clip = altar.sacrificeSuccess;
                    altar.altarAudioSource.Play();
                    altar.music.PlayDelayed(20);
                }

                else
                {
                    altar._display.SetFood(quest._currentFoodCount, quest._foodRequested);
                    altar._display.SetStone(quest._currentStoneCount, quest._stoneRequested);
                    altar._display.SetWood(quest._currentWoodCount, quest._woodRequested);
                    altar._display.SetVillager(quest._currentVillagerCount, quest._villagerRequested);
                    altar._display.SetTime(quest._remainingTime);

                    if (quest._displayed == 0)
                    {
                        altar._display.gameObject.SetActive(true);
                        quest._displayed = 1;
                    }
                    
                    PostUpdateCommands.SetComponent(entity, quest);
                }
            }
        }

        private bool IsQuestSucceed(AltarQuest quest)
        {
            return ((quest._currentFoodCount >= quest._foodRequested) &&
                    (quest._currentStoneCount >= quest._stoneRequested) &&
                    (quest._currentVillagerCount >= quest._villagerRequested) &&
                    (quest._currentWoodCount >= quest._woodRequested));

        }
    }
}