using Pandora.Data;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;

namespace Pandora
{
    public class KeySystem : ComponentSystem
    {
        public struct EscapeKeyGameStates
        {
            public readonly int Length;
            [ReadOnly] public EntityArray _entities;
            [ReadOnly] public ComponentDataArray<GameState> _gameStates;
            [ReadOnly] public SubtractiveComponent<OnEscapeKeyUp> _onEscapeKeyUps;
        }
        
        [Inject] private EscapeKeyGameStates _escapeKeyGameStates;
        
        public struct NotAnyKeyGameStates
        {
            public readonly int Length;
            [ReadOnly] public EntityArray _entities;
            [ReadOnly] public ComponentDataArray<GameState> _gameStates;
            [ReadOnly] public SubtractiveComponent<OnAnyKeyUp> _onAnyKeyUps;
        }
        
        [Inject] private NotAnyKeyGameStates _notAnyKeyGameStates;
        
        public struct AnyKeyGameStates
        {
            public readonly int Length;
            [ReadOnly] public EntityArray _entities;
            [ReadOnly] public ComponentDataArray<GameState> _gameStates;
            [ReadOnly] public ComponentDataArray<OnAnyKeyUp> _onAnyKeyUps;
        }
        
        [Inject] private AnyKeyGameStates _anyKeyGameStates;
        
        protected override void OnUpdate()
        {
            if (_escapeKeyGameStates.Length > 0)
            {
                if (Input.GetKeyUp(KeyCode.Escape))
                {
                    PostUpdateCommands.AddComponent(_escapeKeyGameStates._entities[0], new OnEscapeKeyUp());
                }
            }

            if (_notAnyKeyGameStates.Length > 0)
            {
                if (Input.anyKeyDown)
                {
                    PostUpdateCommands.AddComponent(_notAnyKeyGameStates._entities[0], new OnAnyKeyUp());
                }
            }

            if (_anyKeyGameStates.Length > 0)
            {
                PostUpdateCommands.RemoveComponent<OnAnyKeyUp>(_anyKeyGameStates._entities[0]);
            }
        }
    }
}
