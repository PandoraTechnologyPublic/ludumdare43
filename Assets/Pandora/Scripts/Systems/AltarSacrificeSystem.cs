﻿using System.Collections;
using System.Collections.Generic;
using Pandora.Components;
using Pandora.Data;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Jobs;

namespace Pandora.Systems
{
    public class AltarSacrificeSystem : ComponentSystem
    {
        public struct Villagers
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentArray<Villager> _villagers;
            public ComponentDataArray<Interacting> _interacting;
            public TransformAccessArray _transforms;
        }

        public struct Altars
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentArray<Altar> _altars;
            public ComponentDataArray<BeingInteracted> _beingInteracted;
            public ComponentDataArray<AltarQuest> _quests;
            public TransformAccessArray _transforms;
        }
        
        public struct IngameStates
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentDataArray<GameState> _gameStates;
            public ComponentDataArray<IngameState> _ingameStates;
        }

        [Inject] private Villagers _villagers;
        [Inject] private Altars _altars;
        [Inject] private IngameStates _ingame;
    
        protected override void OnUpdate()
        {
            for (int villagerIndex = 0; villagerIndex < _villagers.Length; villagerIndex++)
            {
                for (int altarIndex = 0; altarIndex < _altars.Length; altarIndex++)
                {
                    Interacting interacting = _villagers._interacting[villagerIndex];
                    Entity altarEntity = _altars._entities[altarIndex];

                    if (interacting._targetEntity == altarEntity)
                    {
                        Transform actorTransform = _villagers._transforms[villagerIndex];
                        Transform altarTransform = _altars._transforms[altarIndex];
                        Entity actorEntity = _villagers._entities[villagerIndex];
                        BeingInteracted beingInteracted = _altars._beingInteracted[altarIndex];
                        AltarQuest quest = _altars._quests[altarIndex];

                        if ((altarTransform.position - actorTransform.position).magnitude < 3.0f &&
                            quest._currentVillagerCount < quest._villagerRequested)
                        {
                            if (beingInteracted._actorCount <= 1)
                            {
                                PostUpdateCommands.RemoveComponent<BeingInteracted>(altarEntity);
                            }
                            else
                            {
                                beingInteracted._actorCount--;
                                PostUpdateCommands.SetComponent(altarEntity, beingInteracted);
                            }

                            /*if (_ingame.Length > 0)
                            {
                                GameState state = _ingame._gameStates[0];
                                state._evilSatisfaction = 1.0f;
                                PostUpdateCommands.SetComponent(_ingame._entities[0], state);
                            }*/

                            quest._currentVillagerCount++;
                            
                            PostUpdateCommands.SetComponent(altarEntity, quest);
                            PostUpdateCommands.AddComponent(actorEntity, new ToDestroy());
                        }
                    }
                }
            }
        }
    }
}
