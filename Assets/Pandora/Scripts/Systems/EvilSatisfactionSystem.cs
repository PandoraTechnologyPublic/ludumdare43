using Pandora.Data;
using Unity.Entities;
using UnityEngine;

namespace Pandora.Systems
{
    public class EvilSatisfactionSystem : ComponentSystem
    {
        public struct GameManagers
        {
            public readonly int Length;
            public ComponentArray<GameManager> _gameManagers;
        }
        
        [Inject] private GameManagers _gameManagers;
        
        public struct IngameStates
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentDataArray<GameState> _gameStates;
            public ComponentDataArray<IngameState> _ingameStates;
        }

        [Inject] private IngameStates _ingameStates;

        public struct EnterGameStates
        {
            public readonly int Length;
            public ComponentDataArray<GameState> _gameStates;
            public ComponentDataArray<EnterGameState> _enterIngameStates;
        }
        
        [Inject] private EnterGameStates _enterGameStates;
        
        protected override void OnUpdate()
        {
            if (_enterGameStates.Length > 0)
            {
                Initialise();
            }
            
            if (_ingameStates.Length > 0)
            {
                /*
                GameState gameState = _ingameStates._gameStates[0];

                gameState._evilSatisfaction -= Time.deltaTime * gameState._evilDrainRate;
                
                PostUpdateCommands.SetComponent(_ingameStates._entities[0], gameState);
                //_ingameStates._gameStates[0] = gameState;*/
            }
        }

        private void Initialise()
        {
            GameManager gameManager = _gameManagers._gameManagers[0];
            GameState gameState = _enterGameStates._gameStates[0];

            gameState._evilDrainRate = 0.3f;
            gameState._evilSatisfaction = 1.0f;

            _enterGameStates._gameStates[0] = gameState;

            //Debug.Log("drainRate= " + gameState._evilDrainRate + " | eviSatisfaction: " + gameState._evilSatisfaction);
        }
    }
}