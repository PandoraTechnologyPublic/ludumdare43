﻿using System.Collections.Generic;
using Pandora.Components;
using Pandora.Data;
using Unity.Entities;
using UnityEngine;
using UnityEngine.AI;

namespace Pandora.Systems
{
    public class VillagerTargetSystem : ComponentSystem
    {
        public struct Villagers
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentArray<Villager> _villagers;
            public ComponentArray<NavMeshAgent> _navAgent;
            public ComponentDataArray<Selected> _selected;
        }

        [Inject] private Villagers _villagers;
        
        protected override void OnUpdate()
        {
            Dictionary<Entity, BeingHarvested> newBeingHarvesteds = new Dictionary<Entity, BeingHarvested>();
            Dictionary<Entity, BeingInteracted> newBeingInteracteds = new Dictionary<Entity, BeingInteracted>();
            
            for (int villagerIndex = 0; villagerIndex < _villagers.Length; villagerIndex++)
            {
                if (Input.GetMouseButtonDown(1))
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hitInfo;

                    if (Physics.Raycast(ray, out hitInfo))
                    {
                        NavMeshAgent navMeshAgent = _villagers._navAgent[villagerIndex];
                        Entity villagerEntity = _villagers._entities[villagerIndex];

                        if (hitInfo.collider.gameObject.name == "GroundMesh")
                        {
                            RemoveAllActions(villagerEntity);

                            Target target;
                            if (!EntityManager.HasComponent<Target>(villagerEntity))
                            {
                                target = new Target();
                                PostUpdateCommands.AddComponent(villagerEntity, target);
                            }
                            else
                            {
                                target = EntityManager.GetComponentData<Target>(villagerEntity);
                            }

                            target._position = hitInfo.point;
                            target._type = TargetType.MOVE;
                            PostUpdateCommands.SetComponent(villagerEntity, target);
                            navMeshAgent.SetDestination(hitInfo.point);
                        }
                        else if (hitInfo.collider.CompareTag("Resource"))
                        {
                            RemoveAllExceptOne<Harvesting>(villagerEntity);
                            Entity resourceEntity = hitInfo.collider.GetComponent<GameObjectEntity>().Entity;

                            if (EntityManager.HasComponent<BeingHarvested>(resourceEntity))
                            {
                                BeingHarvested beingHarvested = EntityManager.GetComponentData<BeingHarvested>(resourceEntity);
                                beingHarvested._harvesterCount++;
                                PostUpdateCommands.SetComponent(resourceEntity, beingHarvested);
                            }
                            else
                            {
                                if (newBeingHarvesteds.ContainsKey(resourceEntity))
                                {
                                    BeingHarvested newBeingHarvested = newBeingHarvesteds[resourceEntity];
                                    ++newBeingHarvested._harvesterCount;
                                    newBeingHarvesteds[resourceEntity] = newBeingHarvested;
                                }
                                else
                                {
                                    newBeingHarvesteds.Add(resourceEntity, new BeingHarvested{_harvesterCount = 0});
                                }
                            }

                            if (EntityManager.HasComponent<Harvesting>(villagerEntity))
                            {
                                Harvesting harvesting = EntityManager.GetComponentData<Harvesting>(villagerEntity);
                                harvesting._targetEntity = resourceEntity;
                                harvesting._position = hitInfo.collider.transform.position;
                                PostUpdateCommands.SetComponent(villagerEntity, harvesting);
                            }
                            else
                            {
                                PostUpdateCommands.AddComponent(villagerEntity, new Harvesting
                                {
                                    _targetEntity = resourceEntity,
                                    _position = hitInfo.collider.transform.position
                                });
                            }
                            
                            Target target = new Target
                            {
                                _position = hitInfo.point,
                                _type = TargetType.ACTION
                            };
                            
                            if (!EntityManager.HasComponent<Target>(villagerEntity))
                            {
                                PostUpdateCommands.AddComponent(villagerEntity, target);
                            }
                            else
                            {
                                PostUpdateCommands.SetComponent(villagerEntity, target);
                            }
                            
                            navMeshAgent.SetDestination(hitInfo.point);
                        }
                        else if (hitInfo.collider.CompareTag("Building"))
                        {
                            RemoveAllExceptOne<Interacting>(villagerEntity);
                            GameObjectEntity buildingEntity = hitInfo.collider.GetComponentInParent<GameObjectEntity>();

                            if (buildingEntity.EntityManager.HasComponent<BeingInteracted>(buildingEntity.Entity))
                            {
                                BeingInteracted beingHarvested = buildingEntity.EntityManager.GetComponentData<BeingInteracted>(buildingEntity.Entity);
                                beingHarvested._actorCount++;
                                PostUpdateCommands.SetComponent(buildingEntity.Entity, beingHarvested);
                            }
                            else
                            {
                                if (newBeingInteracteds.ContainsKey(buildingEntity.Entity))
                                {
                                    BeingInteracted newBeingInteracted = newBeingInteracteds[buildingEntity.Entity];
                                    ++newBeingInteracted._actorCount;
                                    newBeingInteracteds[buildingEntity.Entity] = newBeingInteracted;
                                }
                                else
                                {
                                    newBeingInteracteds.Add(buildingEntity.Entity, new BeingInteracted(){_actorCount = 0});
                                }
                            }

                            if (EntityManager.HasComponent<Interacting>(villagerEntity))
                            {
                                Interacting interacting = EntityManager.GetComponentData<Interacting>(villagerEntity);
                                interacting._targetEntity = buildingEntity.Entity;
                                PostUpdateCommands.SetComponent(villagerEntity, interacting);
                            }
                            else
                            {
                                PostUpdateCommands.AddComponent(villagerEntity, new Interacting{_targetEntity = buildingEntity.Entity});
                            }
                            
                            Target target = new Target
                            {
                                _position = hitInfo.point,
                                _type = TargetType.ACTION
                            };
                            
                            if (!EntityManager.HasComponent<Target>(villagerEntity))
                            {
                                PostUpdateCommands.AddComponent(villagerEntity, target);
                            }
                            else
                            {
                                PostUpdateCommands.SetComponent(villagerEntity, target);
                            }
                            
                            navMeshAgent.SetDestination(hitInfo.point);
                        }
                    }
                }
            }
            
            foreach (KeyValuePair<Entity,BeingHarvested> newBeingHarvested in newBeingHarvesteds)
            {
                PostUpdateCommands.AddComponent(newBeingHarvested.Key, newBeingHarvested.Value);
            }
            
            foreach (KeyValuePair<Entity,BeingInteracted> newBeingInteracted in newBeingInteracteds)
            {
                PostUpdateCommands.AddComponent(newBeingInteracted.Key, newBeingInteracted.Value);
            }
        }

        private void RemoveAllActions(Entity entity)
        {
            if (EntityManager.HasComponent<Harvesting>(entity))
            {
                PostUpdateCommands.RemoveComponent<Harvesting>(entity);
            }
            
            if (EntityManager.HasComponent<Interacting>(entity))
            {
                PostUpdateCommands.RemoveComponent<Interacting>(entity);
            }
        }

        private void RemoveAllExceptOne<TYPE>(Entity entity)
        {
            if (EntityManager.HasComponent<Harvesting>(entity) && typeof(Harvesting) != typeof(TYPE))
            {
                PostUpdateCommands.RemoveComponent<Harvesting>(entity);
            }
            
            if (EntityManager.HasComponent<Interacting>(entity) && typeof(Interacting) != typeof(TYPE))
            {
                PostUpdateCommands.RemoveComponent<Interacting>(entity);
            }
        }
    }
}