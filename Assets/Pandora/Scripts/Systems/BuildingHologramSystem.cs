﻿using System.Collections.Generic;
using Pandora;
using Pandora.Components;
using Pandora.Data;
using Pandora.MonoBehaviours;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;
using UnityEngine.Jobs;

public class BuildingHologramSystem : ComponentSystem
{
    public struct GameManagers
    {
        public readonly int Length;
        public ComponentArray<GameManager> _gameManagers;
    }

    [Inject] private GameManagers _gameManagers;
    
    public struct IngameStates
    {
        public readonly int Length;
        public ComponentDataArray<GameState> _gameStates;
        public ComponentDataArray<IngameState> _ingameStates;
    }

    [Inject] private IngameStates _ingameStates;
    
    public struct SelectedBuildings
    {
        public readonly int Length;
        public ComponentDataArray<SelectedBuilding> _selectedBuildings;
        public ComponentDataArray<BuildingId> _buildings;
        public ComponentDataArray<Position> _positions;
    }

    [Inject] private SelectedBuildings _selectedBuildings;
    
    private Building _currentSelectedBuilding;

    protected override void OnUpdate()
    {
        if (_ingameStates.Length > 0)
        {
            bool shouldDestroyHologram = false;
            
            if (_selectedBuildings.Length > 0)
            {
                BuildingId selectedBuildingId = _selectedBuildings._buildings[0];
                
                if (selectedBuildingId._id < 0)
                {
                    if (_currentSelectedBuilding != null)
                    {
                        shouldDestroyHologram = true;
                    }
                }
                else
                {
                    Position selectedBuildingsPosition = _selectedBuildings._positions[0];
                    SelectedBuilding selectedBuilding = _selectedBuildings._selectedBuildings[0];
                    
                    if ((_currentSelectedBuilding != null) && (_currentSelectedBuilding.buildingId != selectedBuildingId._id))
                    {
                        shouldDestroyHologram = true;
                    }
                    
                    GameManager gameManager = _gameManagers._gameManagers[0];
                    
                    if (_currentSelectedBuilding == null)
                    {
                        Building selectedBuildingPrefab = gameManager.GetBuildingPrefabById(selectedBuildingId._id);
                        
                        _currentSelectedBuilding = GameObject.Instantiate(selectedBuildingPrefab);
                        _currentSelectedBuilding.GetComponent<NavMeshObstacle>().enabled = false;
                        
                        Entity entity = _currentSelectedBuilding.GetComponent<GameObjectEntity>().Entity;
                        BoxCollider[] boxColliders = _currentSelectedBuilding.GetComponentsInChildren<BoxCollider>();

                        foreach (BoxCollider boxCollider in boxColliders)
                        {
                            if (boxCollider != null)
                            {
                                boxCollider.enabled = false;
                            }
                        }

                        Spawner spawner = _currentSelectedBuilding.GetComponentInChildren<Spawner>();

                        if (spawner != null)
                        {
                            spawner.gameObject.SetActive(false);
                        }

                        PostUpdateCommands.AddComponent(entity, new BuildingHologram{_buildingId = selectedBuildingId._id});
                    }
                    
                    _currentSelectedBuilding.transform.position = selectedBuildingsPosition.Value;
                    
                    foreach (MeshRenderer meshRenderer in _currentSelectedBuilding.GetComponentsInChildren<MeshRenderer>())
                    {
                        if (selectedBuilding._isAllowed == 0)
                        {
                            meshRenderer.material = gameManager._notAllowedHologramMaterial;
                        }
                        else
                        {
                            meshRenderer.material = gameManager._allowedHologramMaterial;
                        }
                    }

                    
                    
                }
            }
            else
            {
                shouldDestroyHologram = true;
            }

            if (shouldDestroyHologram)
            {
                if (_currentSelectedBuilding != null)
                {
                    GameObject.Destroy(_currentSelectedBuilding.gameObject);
                    _currentSelectedBuilding = null;
                }
            }
        }
    }
}
