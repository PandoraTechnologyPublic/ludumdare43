﻿using System.Collections;
using System.Collections.Generic;
using Pandora.Components;
using Pandora.Data;
using Pandora.MonoBehaviours;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Jobs;

namespace Pandora.Systems
{
    public class BarracksConversionSystem : ComponentSystem
    {
        public struct Villagers
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentArray<Villager> _villagers;
            public SubtractiveComponent<SoldierData> _soldierData;
            public ComponentDataArray<Interacting> _interacting;
            public TransformAccessArray _transforms;
        }

        public struct Barracks
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentArray<Building> _buildings;
            public ComponentDataArray<BeingInteracted> _beingInteracted;
            public TransformAccessArray _transforms;
        }
        
        public struct IngameStates
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentDataArray<GameState> _gameStates;
            public ComponentDataArray<IngameState> _ingameStates;
        }

        [Inject] private Villagers _villagers;
        [Inject] private Barracks _barracks;
        [Inject] private IngameStates _ingame;
    
        protected override void OnUpdate()
        {
            for (int villagerIndex = 0; villagerIndex < _villagers.Length; villagerIndex++)
            {
                for (int buildingIndex = 0; buildingIndex < _barracks.Length; buildingIndex++)
                {
                    Interacting interacting = _villagers._interacting[villagerIndex];
                    Entity barracksEntity = _barracks._entities[buildingIndex];

                    if ((interacting._targetEntity == barracksEntity) && (_barracks._buildings[buildingIndex].buildingName == "Barracks"))
                    {
                        Transform actorTransform = _villagers._transforms[villagerIndex];
                        Transform altarTransform = _barracks._transforms[buildingIndex];
                        Entity actorEntity = _villagers._entities[villagerIndex];
                        BeingInteracted beingInteracted = _barracks._beingInteracted[buildingIndex];

                        if ((altarTransform.position - actorTransform.position).magnitude < 3.0f)
                        {
                            if (beingInteracted._actorCount <= 1)
                            {
                                PostUpdateCommands.RemoveComponent<BeingInteracted>(barracksEntity);
                            }
                            else
                            {
                                beingInteracted._actorCount--;
                                PostUpdateCommands.SetComponent(barracksEntity, beingInteracted);
                            }

                            PostUpdateCommands.AddComponent(actorEntity, new SoldierData());
                        }
                    }
                }
            }
        }
    }
}
