using System;
using Pandora.Components;
using Pandora.MonoBehaviours;
using Unity.Entities;
using UnityEngine;
using UnityEngine.AI;

namespace Pandora
{
    public class VillagerAnimationSystem : ComponentSystem
    {
        public struct AnimatedVillagers
        {
            public readonly int Length;
            public ComponentArray<Villager> _villagers;
            public ComponentArray<ResourceStorage> _storages;
            public ComponentArray<Animator> _animators;
            public ComponentArray<NavMeshAgent> _navMeshAgents;
        }

        [Inject] private AnimatedVillagers _animatedVillagers;
        
        protected override void OnUpdate()
        {
            for (int animatedVillagerIndex = 0; animatedVillagerIndex < _animatedVillagers.Length; animatedVillagerIndex++)
            {
                NavMeshAgent animatedVillagerNavMeshAgent = _animatedVillagers._navMeshAgents[animatedVillagerIndex];
                Animator animatedVillagerAnimator = _animatedVillagers._animators[animatedVillagerIndex];

                Villager animatedVillager = _animatedVillagers._villagers[animatedVillagerIndex];
                if (animatedVillagerNavMeshAgent.velocity.magnitude > 0.0f)
                {
                    string currentAnimation = "ToWalk";
                                
                    PlayAnimation(animatedVillager, currentAnimation, animatedVillagerAnimator);
                }
                else
                {
                    if (_animatedVillagers._storages[animatedVillagerIndex]._storedValue > 0)
                    {
                        switch (_animatedVillagers._storages[animatedVillagerIndex]._type)
                        {
                            case ResourceType.WOOD:
                            {
                                string currentAnimation = "ToCutWood";
                                
                                PlayAnimation(animatedVillager, currentAnimation, animatedVillagerAnimator);

                                break;
                        }
                            case ResourceType.STONE:
                            {
                                string currentAnimation = "ToCollectStone";
                                
                                PlayAnimation(animatedVillager, currentAnimation, animatedVillagerAnimator);
                                break;
                            }
                            case ResourceType.FOOD:
                            {
                                string currentAnimation = "ToHarvestFood";
                                
                                PlayAnimation(animatedVillager, currentAnimation, animatedVillagerAnimator);
                                break;
                            }
                        }
                    }
                    else
                    {
                        string currentAnimation = "ToIdle";
                                
                        PlayAnimation(animatedVillager, currentAnimation, animatedVillagerAnimator);
                    }
                }
            }
        }

        private static void PlayAnimation(Villager animatedVillager, string currentAnimation, Animator animatedVillagerAnimator)
        {
            if (animatedVillager._currentAnimation != currentAnimation)
            {
                animatedVillager._currentAnimation = currentAnimation;

                animatedVillagerAnimator.SetTrigger(currentAnimation);
            }
        }
    }
}
