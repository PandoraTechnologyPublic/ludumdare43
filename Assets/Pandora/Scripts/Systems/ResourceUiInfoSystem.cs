using Pandora.Data;
using Pandora.MonoBehaviours;
using Pandora.UI;
using Unity.Entities;

namespace Pandora.Systems
{
    public class ResourceUiInfoSystem : ComponentSystem
    {
        public struct Resources
        {
            public readonly int Length;
            public ComponentDataArray<Selected> _selected;
            public SubtractiveComponent<VillagerData> _villagerData;
            public ComponentArray<ResourceStorage> _resourceStorages;
        }
        
        [Inject] private Resources _resources;

        public struct ResourceUi
        {
            public readonly int Length;
            public ComponentArray<ResourceUiLayout> _resourceUiLayout;
        }
        
        [Inject] private ResourceUi _resourceUis;

        protected override void OnUpdate()
        {
            if (_resourceUis.Length == 1)
            {
                if (_resources.Length == 1)
                {
                    ResourceUiLayout resourceUiLayout = _resourceUis._resourceUiLayout[0];
                    
                    ResourceStorage villagersResourceStorage = _resources._resourceStorages[0];
                    resourceUiLayout._resourceStorageText.SetText(villagersResourceStorage._type + " : " + villagersResourceStorage._storedValue);

                    resourceUiLayout._resourceInfoPanel.gameObject.SetActive(true);
                }
                else
                {
                    _resourceUis._resourceUiLayout[0]._resourceInfoPanel.gameObject.SetActive(false);
                }
            }
        }
    }
}