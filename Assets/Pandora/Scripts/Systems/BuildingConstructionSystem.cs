﻿using Pandora;
using Pandora.Components;
using Pandora.Data;
using Pandora.MonoBehaviours;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.EventSystems;

public class BuildingConstructionSystem : ComponentSystem
{
    public struct IngameStates
    {
        public readonly int Length;
        public ComponentDataArray<GameState> _gameStates;
        public ComponentDataArray<IngameState> _ingameStates;
    }

    [Inject] private IngameStates _ingameStates;
    
    public struct GameManagers
    {
        public readonly int Length;
        public ComponentArray<GameManager> _gameManagers;
    }

    [Inject] private GameManagers _gameManagers;
    
    public struct ResourceStates
    {
        public readonly int Length;
        public ComponentDataArray<ResourceState> _resourceStates;
    }
    
    [Inject] private ResourceStates _resourceStates;
    
    public struct SelectedBuildings
    {
        public readonly int Length;
        public EntityArray _entities;
        public ComponentDataArray<SelectedBuilding> _selectedBuildings;
        public ComponentDataArray<BuildingId> _buildings;
        public ComponentDataArray<Position> _positions;
         }

    [Inject] private SelectedBuildings _selectedBuildings;

    private int _groundLayerMask;

    protected override void OnStartRunning()
    {
        _groundLayerMask = LayerMask.GetMask("Ground");
    }

    protected override void OnUpdate()
    {
        if (_ingameStates.Length > 0)
        {
            if (_selectedBuildings.Length > 0)
            {
                if (Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject())
                {
                    Entity selectedBuildingsEntity = _selectedBuildings._entities[0];

                    if (_selectedBuildings._selectedBuildings[0]._isAllowed == 1)
                    {
                        // Generate building on ground
                        int buildingId = _selectedBuildings._buildings[0]._id;
                        Building buildingPrefab = _gameManagers._gameManagers[0].GetBuildingPrefabById(buildingId);
                        // Cost impact
                        ResourceState resourceState = _resourceStates._resourceStates[0];
                        BuildingCost buildCost = buildingPrefab.GetComponent<BuildingCost>();
                        resourceState._woodQuantityCurrent -= buildCost._woodCost;
                        resourceState._stoneQuantityCurrent -= buildCost._stoneCost;
                        _resourceStates._resourceStates[0] = resourceState;

                        float3 snappedPosition = _selectedBuildings._positions[0].Value;
                        Building buildingInstance = GameObject.Instantiate(buildingPrefab, snappedPosition, Quaternion.identity); // THIS INVALIDATES INJECTED ARRAYS
                        GameObjectEntity buildingEntityComponent = buildingInstance.GetComponent<GameObjectEntity>();
                        PostUpdateCommands.AddComponent(buildingEntityComponent.Entity, new BuildingId(buildingId));

                        PostUpdateCommands.DestroyEntity(selectedBuildingsEntity);
                    }
                }
            }
        }
    }
}
