﻿using Pandora.Data;
using Pandora.MonoBehaviours;
using TMPro;
using Unity.Entities;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Jobs;

namespace Pandora.Systems
{
    public class ResourceStorageBuildingSystem : ComponentSystem
    {
        public struct Harvesters
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentDataArray<Harvesting> _harvesting;
            public ComponentArray<ResourceStorage> _resourceStorages;
            public ComponentArray<ResourceGatherDisplayerPrefab> _resourceDisplayers;
            public ComponentArray<NavMeshAgent> _navAgents;
            public TransformAccessArray _transforms;
        }

        public struct ResourceStorageBuildings
        {
            public readonly int Length;
            public EntityArray _entitites;
            public ComponentDataArray<ResourceStorageBuilding> _rsb;
            public TransformAccessArray _transforms;
        }
        
        public struct ResourceStates
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentDataArray<ResourceState> _resourceStates;
        }

        [Inject] private Harvesters _harvesters;
        [Inject] private ResourceStorageBuildings _buildings;
        [Inject] private ResourceStates _resourceStates;
        
        protected override void OnUpdate()
        {
            if (_resourceStates.Length <= 0)
            {
                return;
            }

            ResourceState resourceState = _resourceStates._resourceStates[0];

            for (int harvesterIndex = 0; harvesterIndex < _harvesters.Length; harvesterIndex++)
            {
                Entity harvesterEntity = _harvesters._entities[harvesterIndex];
                Harvesting harvestingElement = _harvesters._harvesting[harvesterIndex];
                ResourceStorage harvesterStorage = _harvesters._resourceStorages[harvesterIndex];
                Transform harvesterTransform = _harvesters._transforms[harvesterIndex];
                NavMeshAgent navAgent = _harvesters._navAgents[harvesterIndex];
                    
                for (int buildingIndex = 0; buildingIndex < _buildings.Length; buildingIndex++)
                {
                    if (harvestingElement._state == HarvestingState.TRANSFERING)
                    {
                        Transform buildingTransform = _buildings._transforms[buildingIndex];

                        if ((harvesterTransform.position - buildingTransform.position).magnitude < 4.0f) //COMPLETELY SUBJECTIVE FUCKING HARDCODED VALUE...
                        {
                            int harvestedValue = harvesterStorage._storedValue;

                            resourceState = HandleTransfer(harvesterStorage, resourceState);
                            
                            DisplayTransferOnUI(
                                _harvesters._transforms[harvesterIndex].position + Vector3.up * 2.0f,
                                harvestedValue,
                                _harvesters._resourceDisplayers[harvesterIndex]._prefab, 
                                harvesterStorage._type
                                );

                            harvestingElement._state = HarvestingState.SEARCHING;
                            
                            PostUpdateCommands.SetComponent(harvesterEntity, harvestingElement);
                            PostUpdateCommands.SetComponent(_resourceStates._entities[0], resourceState);
                            
                            navAgent.SetDestination(harvestingElement._position);
                        }
                    }
                }
                
            }
        }
        
        private ResourceState HandleTransfer(ResourceStorage resourceStorage, ResourceState resourceState)
        {
            switch (resourceStorage._type)
            {
                case ResourceType.FOOD:
                {
                    resourceState._foodQuantityCurrent += resourceStorage._maxValue;
                    resourceStorage._storedValue = 0;
                    break;
                }
                case ResourceType.WOOD:
                {
                    resourceState._woodQuantityCurrent += resourceStorage._maxValue;
                    resourceStorage._storedValue = 0;
                    break;
                }
                case ResourceType.STONE:
                {
                    resourceState._stoneQuantityCurrent += resourceStorage._maxValue;
                    resourceStorage._storedValue = 0;
                    break;
                }
                default:
                {
                    Debug.LogWarning("Unknown ResourceType: " + resourceStorage._type);
                    break;
                }
            }

            return resourceState;
        }


        private void DisplayTransferOnUI(Vector3 position, int count, GameObject displayerPrefab, ResourceType resourceType)
        {
            GameObject displayerObject = GameObject.Instantiate(displayerPrefab);
            TextMeshPro displayer = displayerObject.GetComponentInChildren<TextMeshPro>();
            
            displayerObject.transform.position = position;

            displayer.text = "+ " + count;

            displayerObject.GetComponent<VillagerAudioSource>().playResourceStock(resourceType);
        }
    }
}