using Pandora.Components;
using Pandora.Data;
using Unity.Entities;
using UnityEngine;

namespace Pandora.Systems
{
    public class VillagerInitialiserSystem : ComponentSystem
    {
        public struct Villagers
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentArray<Villager> _villagers;
            public SubtractiveComponent<VillagerData> _villagerData;
        }

        [Inject] private Villagers _villagers;
        
        protected override void OnUpdate()
        {
            for (int villagerIndex = 0; villagerIndex < _villagers.Length; villagerIndex++)
            {
                PostUpdateCommands.AddComponent(_villagers._entities[villagerIndex], new VillagerData{xp = 0, level = 1});
            }
        }
    }
}