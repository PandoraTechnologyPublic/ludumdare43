using Pandora.Data;
using Unity.Entities;
using UnityEngine;
using Pandora.MonoBehaviours;
using TMPro;
using UnityEngine.AI;
using UnityEngine.Jobs;

namespace Pandora.Systems
{
    public class ResourceTransferSystem : ComponentSystem
    {
        public struct Harvesters
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentDataArray<Harvesting> _harvesting; // FIXME replace with Tranfering state instead of Harvesting
            public ComponentArray<ResourceStorage> _resourceStorages;
            public ComponentArray<ResourceGatherDisplayerPrefab> _resourceDisplayers;
            public ComponentArray<NavMeshAgent> _navAgents;
            public TransformAccessArray _transforms;
        }

        public struct ResourceStorageBuildings
        {
            public readonly int Length;
            public ComponentDataArray<ResourceStorageBuilding> _rsb;
            public TransformAccessArray _transforms;
        }

        public struct ResourceStates
        {
            public readonly int Length;
            public ComponentDataArray<ResourceState> _resourceStates;
        }

        [Inject] private Harvesters _harvesters;
        [Inject] private ResourceStates _resourceStates;
        [Inject] private ResourceStorageBuildings _resourceStorageBuildings;

        protected override void OnUpdate()
        {
            // Only one ResourceState is foreseen
            ResourceState resourceState = _resourceStates._resourceStates[0];

            for (int harvesterIndex = 0; harvesterIndex < _harvesters.Length; harvesterIndex++)
            {
                Entity harvesterEntity = _harvesters._entities[harvesterIndex];
                Transform harvesterTransform = _harvesters._transforms[harvesterIndex];
                ResourceStorage resourceStorage = _harvesters._resourceStorages[harvesterIndex];
                Harvesting harvestingElement = _harvesters._harvesting[harvesterIndex];
                NavMeshAgent navMesh = _harvesters._navAgents[harvesterIndex];
                
                // if the Harvester has collected a maxValue then transfer it !
                if (resourceStorage._storedValue >= resourceStorage._maxValue)
                {
                    if (_resourceStorageBuildings.Length <= 0)
                    {
                        PostUpdateCommands.RemoveComponent<Harvesting>(harvesterEntity);
                    }
                    else
                    {
                        harvestingElement._state = HarvestingState.TRANSFERING;

                        int closestRSB = 0;
                        float closestRSBDistance = -1.0f;
                        
                        for (int rsbIndex = 0; rsbIndex < _resourceStorageBuildings.Length; rsbIndex++)
                        {
                            Transform rsbTransform = _resourceStorageBuildings._transforms[rsbIndex];

                            float distance = (rsbTransform.position - harvesterTransform.position).magnitude;

                            if (closestRSBDistance > distance || closestRSBDistance < 0)
                            {
                                closestRSBDistance = distance;
                                closestRSB = rsbIndex;
                            }
                        }
                        
                        navMesh.SetDestination(_resourceStorageBuildings._transforms[closestRSB].position);
                        
                        PostUpdateCommands.SetComponent(harvesterEntity, harvestingElement);
                    }
                }
            }
            _resourceStates._resourceStates[0] = resourceState;
        }

        
    }
}