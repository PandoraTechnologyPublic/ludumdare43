using Pandora.Data;
using Pandora.Components;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Jobs;
using System;

namespace Pandora.Systems
{
    public class PopulationHostingSystem : ComponentSystem
    {
        public struct Populations
        {
            public readonly int Length;
            public ComponentArray<Villager> _villagers;
        }

        public struct PopulationHostings
        {
            public readonly int Length;
            public ComponentArray<PopulationHosting> _populationHostings;
            public SubtractiveComponent<BuildingHologram> _buildingHologram;
        }

        public struct ResourceStates
        {
            public readonly int Length;
            public ComponentDataArray<ResourceState> _resourceStates;
        }

        [Inject] private PopulationHostings _hostings;
        [Inject] private ResourceStates _resourceStates;
        [Inject] private Populations _populations;

        protected override void OnUpdate()
        {
            // Only one ResourceState is foreseen
            ResourceState resourceState = _resourceStates._resourceStates[0];
            resourceState._populationQuantityCurrent = _populations.Length;
            resourceState._populationQuantityMax = 0;

            for (int hostingIndex = 0; hostingIndex != _hostings.Length; hostingIndex++)
            {
                resourceState._populationQuantityMax += _hostings._populationHostings[hostingIndex]._maxPopulationHosting;
            }
            _resourceStates._resourceStates[0] = resourceState;
        }
    }
}