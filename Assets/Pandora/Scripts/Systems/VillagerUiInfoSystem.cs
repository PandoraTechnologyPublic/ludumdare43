using Pandora.Data;
using Pandora.MonoBehaviours;
using Pandora.UI;
using Unity.Entities;

namespace Pandora.Systems
{
    public class VillagerUiInfoSystem : ComponentSystem
    {
        public struct Villagers
        {
            public readonly int Length;
            public ComponentDataArray<Selected> _selected;
            public ComponentDataArray<VillagerData> _villagerData;
            public ComponentArray<ResourceStorage> _resourceStorages;
        }
        
        [Inject] private Villagers _villagers;

        public struct VillagerUi
        {
            public readonly int Length;
            public ComponentArray<VillagerUiLayout> _villagerUiLayout;
        }
        
        [Inject] private VillagerUi _villagerUis;

        protected override void OnUpdate()
        {
            if (_villagerUis.Length == 1)
            {
                if (_villagers.Length == 1)
                {
                    VillagerUiLayout villagerUiLayout = _villagerUis._villagerUiLayout[0];
                    
                    VillagerData villagerData = _villagers._villagerData[0];
                    villagerUiLayout._villagerLevelText.SetText("Level : " + villagerData.level);
                    villagerUiLayout._villagerXpText.SetText("Xp : " + villagerData.xp + "/" + villagerData.nextLevelXp);
                    
                    ResourceStorage villagersResourceStorage = _villagers._resourceStorages[0];
                    villagerUiLayout._villagerStorageText.SetText(villagersResourceStorage._type + " : " + villagersResourceStorage._storedValue);

                    villagerUiLayout._villagerInfoPanel.gameObject.SetActive(true);
                }
                else
                {
                    _villagerUis._villagerUiLayout[0]._villagerInfoPanel.gameObject.SetActive(false);
                }
            }
        }
    }
}