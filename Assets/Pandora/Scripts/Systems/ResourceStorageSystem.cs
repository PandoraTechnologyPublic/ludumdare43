using Pandora.Data;
using Pandora.Components;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Jobs;
using System;
using Pandora.MonoBehaviours;
using TMPro;
using UnityEngine.AI;

namespace Pandora.Systems
{
    public class ResourceStorageSystem : ComponentSystem
    {
        public struct Harvesters
        {
            public readonly int Length;
            public ComponentArray<Villager> _villages;
            public ComponentDataArray<VillagerData> _villagerData;
            public ComponentArray<ResourceGatherDisplayerPrefab> _resourceDisplayers;
            public ComponentDataArray<Harvesting> _harvesting;
            public ComponentArray<ResourceStorage> _resourceStorages;
            public TransformAccessArray _transforms;
            public ComponentArray<NavMeshAgent> _navAgents;
        }

        public struct HarvestedTargets
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentDataArray<BeingHarvested> _beingHarvested;
            public ComponentArray<ResourceStorage> _resourceStorages;
            public TransformAccessArray _transforms;
        }

        [Inject] private Harvesters _harvesters;
        [Inject] private HarvestedTargets _harvestedTargets;

        protected override void OnUpdate()
        {
            for (int harvesterIndex = 0; harvesterIndex != _harvesters.Length; harvesterIndex++)
            {
                for (int harvestedIndex = 0; harvestedIndex != _harvestedTargets.Length; harvestedIndex++)
                {
                    if (_harvesters._harvesting[harvesterIndex]._targetEntity == _harvestedTargets._entities[harvestedIndex] &&
                        _harvesters._harvesting[harvesterIndex]._state == HarvestingState.HARVESTING)
                    {
                        // We have match the targeted BeingCutTree by the CuttingVillager
                        // we still need to check if it can interact (or not) based on their relative transform
                        Transform harvesterTransform = _harvesters._transforms[harvesterIndex];
                        Transform harvestedTransform = _harvestedTargets._transforms[harvestedIndex];
                        float distance = Vector3.Distance(harvesterTransform.position, harvestedTransform.position);
                        
                        // If tree is near enough then cut it !
                        if (Math.Abs(distance) < 2.0f &&
                            _harvestedTargets._resourceStorages[harvestedIndex]._storedValue > 0 &&
                            _harvesters._resourceStorages[harvesterIndex]._storedValue < _harvesters._resourceStorages[harvesterIndex]._maxValue) // FIXME hardcoded radius
                        {
                            // Villager look at the tree
                            _harvesters._navAgents[harvesterIndex].ResetPath();
                            harvesterTransform.LookAt(harvestedTransform);
                            _harvesters._transforms[harvesterIndex] = harvesterTransform;

                            int harvestedUnits = HandleTransfer(
                                _harvestedTargets._resourceStorages[harvestedIndex],
                                _harvesters._resourceStorages[harvesterIndex],
                                Time.deltaTime
                                );

                            if (harvestedUnits > 0)
                            {
                                VillagerData villagerData = _harvesters._villagerData[harvesterIndex];
                                
                                ++villagerData.xp;
                                _harvesters._villagerData[harvesterIndex] = villagerData;
                                
                                DisplayTransferOnUI(harvesterTransform.position + Vector3.up * 1f, harvestedUnits,
                                    _harvesters._resourceDisplayers[harvesterIndex]._prefab,
                                    _harvestedTargets._resourceStorages[harvestedIndex]._type);
                                
                            }
                        }
                        else if (_harvestedTargets._resourceStorages[harvestedIndex]._storedValue > 0 &&
                                 _harvesters._resourceStorages[harvesterIndex]._storedValue < _harvesters._resourceStorages[harvesterIndex]._maxValue &&
                                 !_harvesters._navAgents[harvesterIndex].hasPath)
                        {
                            _harvesters._navAgents[harvesterIndex].SetDestination(harvestedTransform.position);
                        }
                    }
                }
            }
        }

        private int HandleTransfer(ResourceStorage targetStorage, ResourceStorage sourceStorage, float deltaTime)
        {
            sourceStorage._deltaTime += deltaTime;

            if (sourceStorage._deltaTime >= 1.0f)
            {
                if (sourceStorage._type != targetStorage._type)
                {
                    sourceStorage._type = targetStorage._type;
                    sourceStorage._storedValue = 0;
                }

                sourceStorage._deltaTime -= 1.0f;
                sourceStorage._storedValue += 1;
                targetStorage._storedValue -= 1;

                return (1);
            }
            else
            {
                return (0);
            }
        }

        private void DisplayTransferOnUI(Vector3 position, int count, GameObject displayerPrefab, ResourceType resourceType)
        {
            GameObject displayerObject = GameObject.Instantiate(displayerPrefab);
            TextMeshPro displayer = displayerObject.GetComponentInChildren<TextMeshPro>();
            displayerObject.transform.position = position;

            /*if (displayer)
            {*/
                displayer.text = String.Format("+{0}", count);
            //}

            VillagerAudioSource audio = displayerObject.GetComponent<VillagerAudioSource>();
            audio.PlayResourceHit(resourceType);
        }
    }
}