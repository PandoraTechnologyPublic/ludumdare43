﻿using Pandora.Components;
using Pandora.Data;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Jobs;

namespace Pandora.Systems
{
    public class VillagerMoverSystem : ComponentSystem
    {
        public struct Villagers
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentArray<Villager> _villagers;
            public ComponentDataArray<Target> _targets;
            public ComponentArray<Transform> _transforms;
        }

        [Inject] private Villagers _villagers;
        
        protected override void OnUpdate()
        {
            for (int villagerIndex = 0; villagerIndex < _villagers.Length; villagerIndex++)
            {
                Target target = _villagers._targets[villagerIndex];

                //Check if the villager is close enough to the target position
                if (target._type == TargetType.MOVE)
                {
                    Transform transform = _villagers._transforms[villagerIndex];
                    
                    if ((transform.position - target._position).magnitude <= 0.1f)
                    {
                        PostUpdateCommands.RemoveComponent<Target>(_villagers._entities[villagerIndex]);
                    }
                }
            }
        }
    }
}