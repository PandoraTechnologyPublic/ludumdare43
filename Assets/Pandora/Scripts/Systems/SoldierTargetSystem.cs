﻿using System.Collections.Generic;
using Pandora.Components;
using Pandora.Data;
using Unity.Entities;
using UnityEngine;
using UnityEngine.AI;

namespace Pandora.Systems
{
    public class SoldierTargetSystem : ComponentSystem
    {
        public struct Soldiers
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentArray<Villager> _villagers;
            public ComponentDataArray<SoldierData> _soldierData;
            public ComponentArray<NavMeshAgent> _navAgent;
            public ComponentDataArray<Selected> _selected;
        }

        [Inject] private Soldiers _soldiers;
        
        protected override void OnUpdate()
        {
            for (int soldierIndex = 0; soldierIndex < _soldiers.Length; soldierIndex++)
            {
                if (Input.GetMouseButtonDown(1))
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hitInfo;

                    if (Physics.Raycast(ray, out hitInfo))
                    {
                        NavMeshAgent navMeshAgent = _soldiers._navAgent[soldierIndex];
                        Entity soldierEntity = _soldiers._entities[soldierIndex];

                        if (hitInfo.collider.gameObject.GetComponent<Villager>() != null)
                        {
                            // Set attack target
                            
                            Target target = new Target
                            {
                                _position = hitInfo.point,
                                _type = TargetType.ACTION
                            };
                            
                            if (!EntityManager.HasComponent<Target>(soldierEntity))
                            {
                                PostUpdateCommands.AddComponent(soldierEntity, target);
                            }
                            else
                            {
                                PostUpdateCommands.SetComponent(soldierEntity, target);
                            }
                            
                            navMeshAgent.SetDestination(hitInfo.point);
                        }
                    }
                }
            }
        }

        private void RemoveAllExceptOne<TYPE>(Entity entity)
        {
            if (EntityManager.HasComponent<Harvesting>(entity) && typeof(Harvesting) != typeof(TYPE))
            {
                PostUpdateCommands.RemoveComponent<Harvesting>(entity);
            }
            
            if (EntityManager.HasComponent<Interacting>(entity) && typeof(Interacting) != typeof(TYPE))
            {
                PostUpdateCommands.RemoveComponent<Interacting>(entity);
            }
        }
    }
}