using Pandora.Data;
using Pandora.Components;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Jobs;
using System;
using Pandora.MonoBehaviours;
using TMPro;

namespace Pandora.Systems
{
    [UpdateAfter(typeof(ResourceTransferSystem))]
    public class ResourceExhaustSystem : ComponentSystem
    {
        public struct HarvestedTargets
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentDataArray<BeingHarvested> _beingHarvested;
            public ComponentArray<ResourceStorage> _resourceStorages;
            public ComponentArray<Animator> _animators;
            public GameObjectArray _gameObjects;

            public TransformAccessArray _transforms;
        }

        public struct Harvesters
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentArray<Villager> _villagers;
            public ComponentDataArray<Harvesting> _harvesting;
        }

        [Inject] private HarvestedTargets _harvestedTargets;
        [Inject] private Harvesters _harvesters;

        protected override void OnUpdate()
        {
            for (int harvestedIndex = 0; harvestedIndex != _harvestedTargets.Length; harvestedIndex++)
            {
                ResourceStorage storage = _harvestedTargets._resourceStorages[harvestedIndex];
                GameObject gameObject = _harvestedTargets._gameObjects[harvestedIndex];
                Entity harvestedEntity = _harvestedTargets._entities[harvestedIndex];
                
                if ( (storage._storedValue == 0) && (gameObject.GetComponent<AnimationAutoDestoy>() == null) )
                {
                    _harvestedTargets._animators[harvestedIndex].enabled = true;
                    _harvestedTargets._gameObjects[harvestedIndex].AddComponent<AnimationAutoDestoy>();
                    PostUpdateCommands.AddComponent(harvestedEntity, new ResourceExhausted());

                    for (int harvesterIndex = 0; harvesterIndex < _harvesters.Length; harvesterIndex++)
                    {
                        Harvesting harvestingElement = _harvesters._harvesting[harvesterIndex];
                        
                        if (harvestingElement._targetEntity == harvestedEntity)
                        {
                            harvestingElement._state = HarvestingState.SEARCHING;
                            PostUpdateCommands.SetComponent(_harvesters._entities[harvesterIndex], harvestingElement);
                            //PostUpdateCommands.RemoveComponent<harvesting>(_harvesters._entities[harvesterIndex]);
                        }
                    }
                }
            }
        }
    }
}