using Pandora.Data;
using Pandora.UI;
using Unity.Entities;

namespace Pandora.Systems.UI
{
    public class ResourceUiSystem : ComponentSystem
    {
        public struct ResourceUiLayouts
        {
            public readonly int Length;
            public ComponentArray<GlobalResourceUiLayout> _resourceUiLayouts;
        }

        [Inject] private ResourceUiLayouts _resourceUiLayouts;
        
        public struct ResourceStates
        {
            public readonly int Length;
            public ComponentDataArray<ResourceState> _resourceStates;
        }

        [Inject] private ResourceStates _resourceStates;
        
        protected override void OnUpdate()
        {
            if (_resourceUiLayouts.Length == 0)
            {
                return;
            }
            // Only one ResourceUiLayout is foreseen
            GlobalResourceUiLayout resourceUiLayout = _resourceUiLayouts._resourceUiLayouts[0];
            // Only one ResourceState is foreseen
            ResourceState resourceState = _resourceStates._resourceStates[0];

            resourceUiLayout.SetPopulationQuantity(resourceState._populationQuantityCurrent, resourceState._populationQuantityMax);
            resourceUiLayout.SetFoodQuantity(resourceState._foodQuantityCurrent, resourceState._foodQuantityMax);
            resourceUiLayout.SetWoodQuantity(resourceState._woodQuantityCurrent, resourceState._woodQuantityMax);
            resourceUiLayout.SetStoneQuantity(resourceState._stoneQuantityCurrent, resourceState._stoneQuantityMax);
        }
    }
}
