using Pandora.Data;
using Unity.Entities;

namespace Pandora.Systems
{
    public class UiInfoDisplaySystem : ComponentSystem
    {
        public struct MainUiLayouts
        {
            public readonly int Length;
            public ComponentArray<MainUiLayout> _mainUiLayouts;
        }

        [Inject] public MainUiLayouts _mainUiLayouts;
        
        public struct GameStates
        {
            public readonly int Length;
            public ComponentDataArray<GameState> _gameStates;
        }

        [Inject] private GameStates _gameStates;
        
        protected override void OnUpdate()
        {
            MainUiLayout mainUiLayout = _mainUiLayouts._mainUiLayouts[0];
            GameState gameState = _gameStates._gameStates[0];
            
            mainUiLayout.SetEvilSatisfaction(gameState._evilSatisfaction);
        }
    }
}
