﻿using System.Collections;
using System.Collections.Generic;
using Pandora.Data;
using Pandora.UI;
using Unity.Entities;
using UnityEngine;

public class AltarUISystem : ComponentSystem
{
    public struct Altars
    {
        public readonly int Length;
        public ComponentArray<Altar> _altars;
        public ComponentArray<AltarUIBinder> _binders;
        public ComponentArray<SelectableEntity> _selectable;
        public ComponentDataArray<Selected> _selected;
        public ComponentDataArray<AltarQuest> _quests;
    }

    public struct ResourceStates
    {
        public readonly int Length;
        public ComponentDataArray<ResourceState> _states;
    }

    [Inject] private Altars _altars;
    [Inject] private ResourceStates _resourceStates;
    
    protected override void OnUpdate()
    {
        if (_resourceStates.Length <= 0)
        {
            return;
        }

        ResourceState state = _resourceStates._states[0];
        for (int altarIndex = 0; altarIndex < _altars.Length; altarIndex++)
        {
            AltarInfoMenu menu = _altars._binders[altarIndex]._menu;
            AltarQuest quest = _altars._quests[altarIndex];
            
            menu.RefreshData(quest, state);
        }
    }
}
