﻿using Pandora.Data;
using Pandora.MonoBehaviours;
using Unity.Entities;
using UnityEngine;

namespace Pandora.Systems.UI
{
    
    public class AltarSacrificeRequestHandlerSystem : ComponentSystem
    {
        public struct Altars
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentArray<Altar> _altars;
            public ComponentDataArray<AltarQuest> _quests;
            public ComponentDataArray<SacrificeRequest> _requests;
        }
        
        public struct ResourceStates
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentDataArray<ResourceState> _states;
        }

        [Inject] private Altars _altars;
        [Inject] private ResourceStates _resourceStates;
        
        protected override void OnUpdate()
        {
            if (_resourceStates.Length <= 0)
            {
                return;
            }

            ResourceState state = _resourceStates._states[0];

            for (int altarIndex = 0; altarIndex < _altars.Length; altarIndex++)
            {
                SacrificeRequest request = _altars._requests[altarIndex];
                AltarQuest quest = _altars._quests[altarIndex];
                
                switch (request._type)
                {
                    case ResourceType.FOOD:
                    {
                        Debug.Log("Request = FOOD");
                        if (state._foodQuantityCurrent >= quest._foodRequested)
                        {
                            state._foodQuantityCurrent -= quest._foodRequested;
                            quest._currentFoodCount = quest._foodRequested;
                        }

                        break;
                    }
                    case ResourceType.STONE:
                    {
                        Debug.Log("Request = STONE");

                        if (state._stoneQuantityCurrent >= quest._stoneRequested)
                        {
                            state._stoneQuantityCurrent -= quest._stoneRequested;
                            quest._currentStoneCount = quest._stoneRequested;
                        }

                        break;
                    }
                    case ResourceType.WOOD:
                    {
                        Debug.Log("Request = WOOD");

                        if (state._woodQuantityCurrent >= quest._woodRequested)
                        {
                            state._woodQuantityCurrent -= quest._woodRequested;
                            quest._currentWoodCount = quest._woodRequested;
                        }

                        break;
                    }
                }
                
                PostUpdateCommands.SetComponent(_altars._entities[altarIndex], quest);
                PostUpdateCommands.SetComponent(_resourceStates._entities[0], state);
                PostUpdateCommands.RemoveComponent<SacrificeRequest>(_altars._entities[altarIndex]);
            }
        }
    }
}