﻿using Pandora.Data;
using Pandora.MonoBehaviours;
using Unity.Entities;
using Unity.Transforms;
using UnityEngine;
using UnityEngine.Assertions;

namespace Pandora.Systems.Ui
{
    [UpdateAfter(typeof(BuildingHologram))]
    public class BuildingSelectionSystem : ComponentSystem
    {
        public struct SelectedBuildings
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentDataArray<SelectedBuilding> _selectedBuildings;
            public ComponentDataArray<BuildingId> _buildings;
        }

        [Inject] private SelectedBuildings _selectedBuildings;
        
        public struct ClickedBuildingButtonGroup
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentDataArray<BuildingId> _buildings;
            public ComponentDataArray<Clicked> _clickedTags;
            public ComponentDataArray<Data.Ui> _uiTags;
        }

        [Inject] private ClickedBuildingButtonGroup _clickedBuildingUis;

        protected override void OnUpdate()
        {
            if (Input.GetMouseButtonUp(1))
            {
                // Cancel current selection
                for (int i = 0; i < _clickedBuildingUis.Length; ++i)
                {
                    PostUpdateCommands.RemoveComponent<Clicked>(_clickedBuildingUis._entities[i]);
                }
                
                if (_selectedBuildings.Length > 0)
                {
                    Entity selectedBuildingsEntity = _selectedBuildings._entities[0];
                    PostUpdateCommands.DestroyEntity(selectedBuildingsEntity);
                }
            }
            else  if (_clickedBuildingUis.Length > 0)
            {
                Assert.AreEqual(1, _clickedBuildingUis.Length);
                
                if (_selectedBuildings.Length == 0)
                {
                    PostUpdateCommands.CreateEntity();
                    PostUpdateCommands.AddComponent(new SelectedBuilding());
                    PostUpdateCommands.AddComponent(new BuildingId{_id = _clickedBuildingUis._buildings[0]._id});
                    PostUpdateCommands.AddComponent(new Position());
                }
                else
                {
                    Entity selectedBuildingsEntity = _selectedBuildings._entities[0];
                    
                    PostUpdateCommands.SetComponent(selectedBuildingsEntity, new BuildingId{_id = _clickedBuildingUis._buildings[0]._id});
                }
            }

            for (int clickedBuildingUiIndex = 0; clickedBuildingUiIndex < _clickedBuildingUis.Length; clickedBuildingUiIndex++)
            {
                PostUpdateCommands.RemoveComponent<Clicked>(_clickedBuildingUis._entities[clickedBuildingUiIndex]);   
            }
        }
    }
}
