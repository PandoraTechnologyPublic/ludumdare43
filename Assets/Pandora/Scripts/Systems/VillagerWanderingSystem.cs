using Pandora.Components;
using Pandora.Data;
using Unity.Entities;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Jobs;

namespace Pandora
{
    public class VillagerWanderingSystem : ComponentSystem
    {
        public struct IdleVillagers
        {
            public readonly int Length;
            public EntityArray Entities;
            public ComponentArray<Villager> _villagers;
            public ComponentArray<NavMeshAgent> _navMeshAgents;
            public TransformAccessArray _transforms;
            public SubtractiveComponent<Harvesting> _nonHarvesting;
            public SubtractiveComponent<Interacting> _nonInteracting;
        }

        [Inject] private IdleVillagers _idleVillagers;

        public struct IdleVillagerButtons
        {
            public readonly int Length;
            public ComponentArray<IdleVillagerButton> _idleVillagerButtons;
            public SubtractiveComponent<Clicked> _nonClicked;
        }

        [Inject] private IdleVillagerButtons _idleVillagerButtons;

        public struct ClickedIdleVillagerButtons
        {
            public readonly int Length;
            public EntityArray Entities;
            public ComponentArray<IdleVillagerButton> _idleVillagerButtons;
            public ComponentDataArray<Clicked> _clickedTags;
        }

        [Inject] private ClickedIdleVillagerButtons _clickedIdleVillagerButtons;
        
        protected override void OnUpdate()
        {
            Vector3 boxCastHalfSizes = new Vector3(0.5f, 0.5f, 0.5f);

            for (int i = 0; i < _idleVillagers.Length; i++)
            {
                NavMeshAgent navMeshAgent = _idleVillagers._navMeshAgents[i];
                Vector3 agentPosition = _idleVillagers._transforms[i].position;
                agentPosition.y = agentPosition.y + 0.5f;

                if (!navMeshAgent.hasPath || navMeshAgent.velocity.magnitude <= 0)
                {
                    Vector3 randomDestination;
                    do
                    {
                        randomDestination = agentPosition + 3 * Random.insideUnitSphere;
                        randomDestination.y = agentPosition.y;
                    } while (Physics.CheckBox(randomDestination, boxCastHalfSizes));
                    navMeshAgent.SetDestination(randomDestination);
                }
            }

            if (_idleVillagerButtons.Length > 0)
            {
                _idleVillagerButtons._idleVillagerButtons[0].SetAmount(_idleVillagers.Length);
            }

            if (_clickedIdleVillagerButtons.Length > 0)
            {
                PostUpdateCommands.RemoveComponent<Clicked>(_clickedIdleVillagerButtons.Entities[0]);

                for (int i = 0; i < _idleVillagers.Length; i++)
                {
                    Entity villagerEntity = _idleVillagers.Entities[i];
                    if (!EntityManager.HasComponent<Clicked>(villagerEntity)      // this first condition is to make sure we're safe
                        && !EntityManager.HasComponent<Selected>(villagerEntity)) // this second condition allows to loop on idle villagers
                    {
                        PostUpdateCommands.AddComponent(villagerEntity, new Clicked());

                        if (!EntityManager.HasComponent<CameraFocused>(villagerEntity))
                        {
                            PostUpdateCommands.AddComponent(villagerEntity, new CameraFocused());
                        }
                        break;
                    }
                }
            }
        }
    }
}
