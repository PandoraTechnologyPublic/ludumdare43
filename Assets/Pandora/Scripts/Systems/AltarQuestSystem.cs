﻿using Pandora.Data;
using Pandora.UI;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Jobs;
using Vector3 = UnityEngine.Vector3;

public class AltarQuestSystem : ComponentSystem
{
    public struct Altars
    {
        public readonly int Length;
        public EntityArray _entities;
        public ComponentArray<Altar> _altars;
        public SubtractiveComponent<AltarQuest> _quests;
        public ComponentDataArray<AltarQuestGenerationState> _generationStates;
        public ComponentArray<AltarUIBinder> _binders;
        public TransformAccessArray _transforms;
    }

    [Inject] private Altars _altars;

    protected override void OnUpdate()
    {
        for (int altarIndex = 0; altarIndex < _altars.Length; altarIndex++)
        {
            Altar altar = _altars._altars[altarIndex];
            Entity entity = _altars._entities[altarIndex];
            Transform transform = _altars._transforms[altarIndex];
            AltarInfoMenu menu = _altars._binders[altarIndex]._menu;
            AltarQuestGenerationState generationState = _altars._generationStates[altarIndex];

            altar._time += Time.deltaTime;
            
            if (altar._state == AltarQuestState.BEGINNING)
            {
                if (altar._time >= altar._firstQuestDelay)
                {
                    AltarQuest quest = CreateQuest(entity, generationState._budget, altar._questTime);
                    altar._state = AltarQuestState.WAITING;
                    altar._time = 0;
                    menu.InitQuest(quest);

                    altar.textOnScreen.gameObject.SetActive(true);
                    altar.textOnScreen.text = "Gods require a sacrifice!!! >:[";
                    altar.textOnScreen.color = new Color32(0xE7, 0x15, 0x15, 0xFF);

                    altar.music.Stop();
                    altar.altarAudioSource.clip = altar.newSacrifice;
                    altar.altarAudioSource.Play();
                }
            }
            
            else if (altar._state == AltarQuestState.WAITING)
            {
                generationState._deltaTime += Time.deltaTime;
                if (generationState._deltaTime >= generationState._increaseTime)
                {
                    generationState._budget += generationState._incrementationFactor;
                    generationState._deltaTime = 0.0f;
                    
                }
                
                if (altar._time >= altar._nextQuestTime)
                {
                    AltarQuest quest = CreateQuest(entity, generationState._budget, altar._questTime);
                    altar._time = 0;
                    menu.InitQuest(quest);

                    altar.textOnScreen.gameObject.SetActive(true);
                    altar.textOnScreen.text = "Gods require another sacrifice!!! >:[";
                    altar.textOnScreen.color = new Color32(0xE7, 0x15, 0x15, 0xFF);

                    altar.music.Stop();
                    altar.altarAudioSource.clip = altar.newSacrifice;
                    altar.altarAudioSource.Play();
                }
                
                PostUpdateCommands.SetComponent(entity, generationState);
            }
        }
    }

    private AltarQuest CreateQuest(Entity altar, int budget, float time = 60)
    {
        AltarQuest quest = new AltarQuest
        {
            _foodRequested = 0,
            _remainingTime = time,
            _stoneRequested = 0,
            _villagerRequested = 0,
            _woodRequested = 0
        };

        int resourceSelection;
        int unitCost;
        
        while (budget > 0)
        {
            resourceSelection = Random.Range(0, 4);

            if (resourceSelection == 0) //Human
            {
                unitCost = 3;
            }
            else
            {
                unitCost = 1;
            }

            if (budget >= unitCost)
            {
                budget -= unitCost;

                switch (resourceSelection)
                {
                    case 0: //Humans
                    {
                        quest._villagerRequested++;
                        break;
                    }
                    case 1: //Wood
                    {
                        quest._woodRequested += 10;
                        break;
                    }
                    case 2: //Food
                    {
                        quest._foodRequested += 10;
                        break;
                    }
                    case 3: //Stone
                    {
                        quest._stoneRequested += 10;
                        break;
                    }
                }
            }
        }

        if (!EntityManager.HasComponent<AltarQuest>(altar))
        {
            PostUpdateCommands.AddComponent(altar, quest);
        }
        else
        {
            PostUpdateCommands.SetComponent(altar, quest);
        }

        return (quest);
    }
}
