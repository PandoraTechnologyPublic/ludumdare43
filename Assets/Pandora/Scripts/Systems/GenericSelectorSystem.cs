﻿using Pandora.Components;
using Pandora.Data;
using Pandora.MonoBehaviours;
using Unity.Entities;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Jobs;

namespace Pandora.Systems
{
    public class GenericSelectorSystem : ComponentSystem
    {
        public struct ClickedEntities
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentArray<SelectableEntity> _selectable;
            public ComponentDataArray<Clicked> _clicked;
        }

        public struct SelectedEntities
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentDataArray<Selected> _selected;
            public ComponentArray<SelectableEntity> _selectable;
            public TransformAccessArray _transforms;
        }

        [Inject] private ClickedEntities _clickedEntities;
        [Inject] private SelectedEntities _selectedEntities;
        
        protected override void OnUpdate()
        {            
            if (_clickedEntities.Length == 0 && _selectedEntities.Length > 0)
            {
                if (Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject())
                {
                    for (int selectedIndex = 0; selectedIndex < _selectedEntities.Length; selectedIndex++)
                    {
                        Entity entity = _selectedEntities._entities[selectedIndex];
                        SelectableEntity selectableEntity = _selectedEntities._selectable[selectedIndex];
                        
                        if (selectableEntity._selectionMesh != null)
                        {
                            selectableEntity._selectionMesh.SetActive(false);
                        }

                        if (selectableEntity._selectionUIInterface != null)
                        {
                            selectableEntity._selectionUIInterface.SetActive(false);
                        }

                        PostUpdateCommands.RemoveComponent<Selected>(entity);
                    }
                }
            }
            
            else if (_clickedEntities.Length > 0 && _selectedEntities.Length > 0)
            {
                for (int selectedIndex = 0; selectedIndex < _selectedEntities.Length; selectedIndex++)
                {
                    Entity entity = _selectedEntities._entities[selectedIndex];
                    SelectableEntity selectableEntity = _selectedEntities._selectable[selectedIndex];
                        
                    if (selectableEntity._selectionMesh != null)
                    {
                        selectableEntity._selectionMesh.SetActive(false);
                    }

                    if (selectableEntity._selectionUIInterface != null)
                    {
                        selectableEntity._selectionUIInterface.SetActive(false);
                    }

                    PostUpdateCommands.RemoveComponent<Selected>(entity);
                }
            }
            
            for (int clickedIndex = 0; clickedIndex < _clickedEntities.Length; clickedIndex++)
            {
                Entity entity = _clickedEntities._entities[clickedIndex];
                SelectableEntity selectableEntity = _clickedEntities._selectable[clickedIndex];
                
                if (!EntityManager.HasComponent<Selected>(entity))
                {
                    if (selectableEntity._selectionMesh != null)
                    {
                        selectableEntity._selectionMesh.SetActive(true);
                    }

                    if (selectableEntity._selectionUIInterface != null)
                    {
                        selectableEntity._selectionUIInterface.SetActive(true);
                    }

                    PostUpdateCommands.AddComponent(entity, new Selected());
                    PostUpdateCommands.RemoveComponent<Clicked>(entity);

                    Villager villager = selectableEntity.GetComponent<Villager>();

                    if (villager != null)
                    {
                        villager.GetComponent<AudioSource>().clip = villager._sounds[Random.Range(0,villager._sounds.Length)];
                        villager.GetComponent<AudioSource>().Play();
                    }
                }
            }
        }
    }
}