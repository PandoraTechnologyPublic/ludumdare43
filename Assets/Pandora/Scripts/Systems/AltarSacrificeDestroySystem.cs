﻿using System.Collections;
using System.Collections.Generic;
using Pandora.Components;
using Pandora.Data;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Jobs;

namespace Pandora.Systems
{
    [UpdateAfter(typeof(AltarSacrificeSystem))]
    public class AltarSacrificeDestroySystem : ComponentSystem
    {
        public struct Villagers
        {
            public readonly int Length;
            public EntityArray _entities;
            public ComponentArray<Villager> _villagers;
            public ComponentDataArray<ToDestroy> _toDestroy;
            public TransformAccessArray _transforms;
        }

        [Inject] private Villagers _villagers;
    
        protected override void OnUpdate()
        {
            for (int villagerIndex = 0; villagerIndex < _villagers.Length; villagerIndex++)
            {
                Transform actorTransform = _villagers._transforms[villagerIndex];
                GameObject.Destroy(actorTransform.gameObject);
            }
        }
    }
}
