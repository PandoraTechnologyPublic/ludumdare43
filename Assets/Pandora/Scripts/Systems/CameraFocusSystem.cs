using Cinemachine;
using Pandora.Data;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Jobs;

namespace Pandora.Systems
{
    public class CameraFocusSystem : ComponentSystem
    {
        public struct CameraGroup
        {
            public readonly int Length;
            public ComponentArray<Camera> _cameras;
        }

        [Inject] private CameraGroup _cameras;

        public struct FocusedObjectGroup
        {
            public readonly int Length;
            public EntityArray Entities;
            public ComponentDataArray<CameraFocused> _cameraFocusedObjects;
            public TransformAccessArray _transforms;
        }

        [Inject] private FocusedObjectGroup _cameraFocusedObjects;

        private int _groundLayerMask;

        protected override void OnStartRunning()
        {
            _groundLayerMask = LayerMask.GetMask("Ground");
        }

        protected override void OnUpdate()
        {
            if (_cameraFocusedObjects.Length > 0)
            {
                Transform cameraTransform = _cameras._cameras[0].transform.parent.GetComponentInChildren<FreeLookCamera>().transform;
                Ray ray = _cameras._cameras[0].ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
                RaycastHit hit;
                
                if (Physics.Raycast(ray, out hit, Mathf.Infinity, _groundLayerMask))
                {
//                    Debug.DrawLine(_cameraFocusedObjects._transforms[0].position, _cameraFocusedObjects._transforms[0].position + Vector3.up, Color.green, 2.0f);
//                    Debug.DrawLine(_cameraFocusedObjects._transforms[0].position, _cameraFocusedObjects._transforms[0].position - cameraTransform.forward * 20, Color.red, 2.0f);
                    cameraTransform.position = _cameraFocusedObjects._transforms[0].position - cameraTransform.forward * 10 + cameraTransform.up * 10;
                }

                PostUpdateCommands.RemoveComponent<CameraFocused>(_cameraFocusedObjects.Entities[0]);
            }
        }
    }
}
