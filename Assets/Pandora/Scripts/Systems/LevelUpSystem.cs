using Pandora.Data;
using Unity.Entities;
using UnityEngine;

namespace Pandora.Systems
{
    public class LevelUpSystem : ComponentSystem
    {
        public struct Villagers
        {
            public readonly int Length;
            public ComponentDataArray<VillagerData> _villagerData;
        }

        [Inject] private Villagers _villagers;

        private static readonly int MAX_LEVEL = 10;
        private static readonly int LEVEL_XP_STEP = 10;
        
        private int[] levelTable = new int[MAX_LEVEL];
        
        protected override void OnCreateManager()
        {
            int maxLevel = 10;
            
            base.OnCreateManager();

            levelTable[0] = 40;
            
            for (int levelIndex = 1; levelIndex < maxLevel; ++levelIndex)
            {
                //https://docs.google.com/spreadsheets/d/1fO8WByH5-bkqk3yPpkAJuUiu0ps8eIDamee-3OWU-Jk/
                levelTable[levelIndex] = levelTable[levelIndex - 1] + levelTable[0] + (LEVEL_XP_STEP * levelIndex);
            }
        }

        protected override void OnUpdate()
        {
            for (int villagerIndex = 0; villagerIndex < _villagers.Length; villagerIndex++)
            {
                VillagerData villagerData = _villagers._villagerData[villagerIndex];

                if (villagerData.xp >= levelTable[villagerData.level - 1])
                {
                    ++villagerData.level;
                }

                villagerData.nextLevelXp = levelTable[villagerData.level - 1];
                
                _villagers._villagerData[villagerIndex] = villagerData;
            }
        }
    }
}