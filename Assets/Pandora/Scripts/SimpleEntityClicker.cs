﻿using Pandora.Data;
using Unity.Entities;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Pandora
{
    public class SimpleEntityClicker : MonoBehaviour
    {
        private void OnMouseUpAsButton()
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                GameObjectEntity gameObjectEntity = GetComponent<GameObjectEntity>();

                if (gameObjectEntity == null)
                {
                    gameObjectEntity = GetComponentInParent<GameObjectEntity>();
                }

                if (gameObjectEntity != null)
                {
                    if (!gameObjectEntity.EntityManager.HasComponent<Clicked>(gameObjectEntity.Entity))
                    {
                        gameObjectEntity.EntityManager.AddComponentData(gameObjectEntity.Entity, new Clicked());
                    }
                }
            }
        }
    }
}
