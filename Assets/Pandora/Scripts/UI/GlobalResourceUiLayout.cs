﻿using UnityEngine;
using TMPro;

namespace Pandora.UI
{
    public class GlobalResourceUiLayout : MonoBehaviour
    {
        [SerializeField] private TMP_Text _populationText;
        [SerializeField] private TMP_Text _foodText;
        [SerializeField] private TMP_Text _woodText;
        [SerializeField] private TMP_Text _stoneText;

        public void SetPopulationQuantity(int currentQuantity, int maxQuantity)
        {
            _populationText.SetText(currentQuantity + "/" + maxQuantity);
        }

        public void SetFoodQuantity(int currentQuantity, int maxQuantity)
        {
            _foodText.SetText(currentQuantity.ToString()/* + "/" + maxQuantity*/);
        }

        public void SetWoodQuantity(int currentQuantity, int maxQuantity)
        {
            _woodText.SetText(currentQuantity.ToString()/* + "/" + maxQuantity*/);
        }

        public void SetStoneQuantity(int currentQuantity, int maxQuantity)
        {
            _stoneText.SetText(currentQuantity.ToString()/* + "/" + maxQuantity*/);
        }
    }
}