using TMPro;
using UnityEngine;

namespace Pandora.UI
{
    public class ResourceUiLayout : MonoBehaviour
    {
        [SerializeField] public RectTransform _resourceInfoPanel;
        [SerializeField] public TMP_Text _resourceStorageText;
    }
}