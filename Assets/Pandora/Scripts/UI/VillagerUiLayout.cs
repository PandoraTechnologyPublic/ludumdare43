using TMPro;
using UnityEngine;

namespace Pandora.UI
{
    public class VillagerUiLayout : MonoBehaviour
    {
        [SerializeField] public RectTransform _villagerInfoPanel;
        [SerializeField] public TMP_Text _villagerStorageText;
        [SerializeField] public TMP_Text _villagerXpText;
        [SerializeField] public TMP_Text _villagerLevelText;
    }
}