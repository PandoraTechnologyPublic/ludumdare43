﻿using Pandora.Components;
using Pandora.Data;
using Pandora.MonoBehaviours;
using Unity.Entities;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Pandora.Ui.ButtonClicker
{
    public class BuildingEntityClicker : ButtonEntityClicker, IPointerEnterHandler, IPointerExitHandler
    {
        [SerializeField] private Building _buildingPrefab;
        [SerializeField] private BuildingMenuTooltip _buildingMenuTooltip;

        protected override void InitializeEntity(Entity entity)
        {
            _gameObjectEntity.EntityManager.AddComponentData(_gameObjectEntity.Entity, new BuildingId(_buildingPrefab.buildingId));
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            _buildingMenuTooltip.Show(_buildingPrefab.buildingName, _buildingPrefab.GetComponent<BuildingCost>(), _buildingPrefab.buildingDescription);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _buildingMenuTooltip.Hide();
        }
    }
}
