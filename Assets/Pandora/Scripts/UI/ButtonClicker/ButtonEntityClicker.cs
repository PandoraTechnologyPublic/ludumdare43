﻿using Pandora.Data;
using Unity.Entities;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora.Ui.ButtonClicker
{
    [RequireComponent(typeof(GameObjectEntity), typeof(Button))]
    public class ButtonEntityClicker : MonoBehaviour
    {
        protected GameObjectEntity _gameObjectEntity;

        private void Awake()
        {
            // Initialize behaviour
            _gameObjectEntity = GetComponent<GameObjectEntity>();
            GetComponent<Button>().onClick.AddListener(OnClick);
        }

        private void OnEnable()
        {
            _gameObjectEntity.EntityManager.AddComponentData(_gameObjectEntity.Entity, new Data.Ui());
            InitializeEntity(_gameObjectEntity.Entity);
        }


        protected void OnClick()
        {
            if (!_gameObjectEntity.EntityManager.HasComponent<Clicked>(_gameObjectEntity.Entity))
            {
                _gameObjectEntity.EntityManager.AddComponentData(_gameObjectEntity.Entity, new Clicked{_frameClickedIndex = Time.frameCount});
            }
        }

        protected virtual void InitializeEntity(Entity entity) { }
    }
}
