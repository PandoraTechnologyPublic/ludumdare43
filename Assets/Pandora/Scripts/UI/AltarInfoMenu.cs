﻿using System;
using Pandora.Data;
using TMPro;
using Unity.Entities;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora.UI
{
    public class AltarInfoMenu : MonoBehaviour
    {
        [SerializeField] private TMP_Text _TMPTime;
        [SerializeField] private TMP_Text _TMPVillager;
        [SerializeField] private TMP_Text _TMPStone;
        [SerializeField] private TMP_Text _TMPWood;
        [SerializeField] private TMP_Text _TMPFood;

        [SerializeField] private GameObject _villagerLine;
        [SerializeField] private GameObject _stoneLine;
        [SerializeField] private GameObject _woodLine;
        [SerializeField] private GameObject _foodLine;

        [SerializeField] private Button _stoneButton;
        [SerializeField] private Button _woodButton;
        [SerializeField] private Button _foodButton;
        
        [SerializeField] private TMP_Text _TMPStoneButton;
        [SerializeField] private TMP_Text _TMPWoodButton;
        [SerializeField] private TMP_Text _TMPFoodButton;
        [SerializeField] private TMP_Text _villagerCount;

        [SerializeField] private GameObject _sacrificePanel;
        [SerializeField] private GameObject _noSacrificePanel;

        [SerializeField] private GameObjectEntity _altarEntity;

        public enum ResourceType
        {
            WOOD,
            STONE,
            FOOD
        };
        
        public void Awake()
        {
            //ToggleSacrificeOrder(false);
            
            _stoneButton.onClick.AddListener(OnSacrificeStoneButtonClicked);
            _woodButton.onClick.AddListener(OnSacrificeWoodButtonClicked);
            _foodButton.onClick.AddListener(OnSacrificeFoodButtonClicked);
        }
        
        public void InitQuest(AltarQuest quest)
        {
            _villagerLine.SetActive(quest._villagerRequested > 0);
            _stoneLine.SetActive(quest._stoneRequested > 0);
            _woodLine.SetActive(quest._woodRequested > 0);
            _foodLine.SetActive(quest._foodRequested > 0);
            
            if (quest._stoneRequested > 0)
            {
                _TMPStone.SetText(String.Format("{0} Stone", quest._stoneRequested));
            }
            if (quest._woodRequested > 0)
            {
                _TMPWood.SetText(String.Format("{0} Wood", quest._woodRequested));
            }
            if (quest._foodRequested > 0)
            {
                _TMPFood.SetText(String.Format("{0} Food", quest._foodRequested));
            }
            if (quest._villagerRequested > 0)
            {
                _TMPVillager.SetText(String.Format("{0} Villagers", quest._villagerRequested));
            }
            
            ToggleSacrificeOrder(true);
        }

        public void RefreshData(AltarQuest quest, ResourceState state)
        {
            _TMPTime.SetText(String.Format(
                "{0:D2}:{1:D2}",
                (int) Math.Floor(quest._remainingTime / 60),
                (int) Math.Floor(quest._remainingTime % 60)));

            if (quest._villagerRequested > 0)
            {
                _TMPVillager.SetText(String.Format("{0} Villagers", quest._villagerRequested));
                if (quest._currentVillagerCount >= quest._villagerRequested)
                {
                    _villagerCount.SetText("Done");
                }
                else
                {
                    _villagerCount.SetText(String.Format("{0} remaining", quest._villagerRequested - quest._currentVillagerCount));
                }
            }
            if (quest._stoneRequested > 0)
            {
                if (state._stoneQuantityCurrent < quest._stoneRequested)
                {
                    _stoneButton.interactable = false;
                    _TMPStoneButton.SetText(String.Format("{0} remaining", quest._stoneRequested));
                }
                else
                {
                    if (quest._currentStoneCount >= quest._stoneRequested)
                    {
                        _stoneButton.interactable = false;
                        _TMPStoneButton.SetText("Done");
                    }
                    else
                    {
                        _stoneButton.interactable = true;
                        _TMPStoneButton.SetText("Sacrifice");
                    }
                }
            }
            if (quest._woodRequested > 0)
            {
                if (state._woodQuantityCurrent < quest._woodRequested)
                {
                    _woodButton.interactable = false;
                    _TMPWoodButton.SetText(String.Format("{0} remaining", quest._woodRequested));
                }
                else
                {
                    if (quest._currentWoodCount >= quest._woodRequested)
                    {
                        _woodButton.interactable = false;
                        _TMPWoodButton.SetText("Done");
                    }
                    else
                    {
                        _woodButton.interactable = true;
                        _TMPWoodButton.SetText("Sacrifice");
                    }
                }
            }
            if (quest._foodRequested > 0)
            {
                if (state._foodQuantityCurrent < quest._foodRequested)
                {
                    _foodButton.interactable = false;
                    _TMPFoodButton.SetText(String.Format("{0} remaining", quest._foodRequested));
                }
                else
                {
                    if (quest._currentFoodCount >= quest._foodRequested)
                    {
                        _foodButton.interactable = false;
                        _TMPFoodButton.SetText("Done");
                    }
                    else
                    {
                        _foodButton.interactable = true;
                        _TMPFoodButton.SetText("Sacrifice");
                    }
                }
            }
        }

        public void ToggleSacrificeOrder(bool toggle)
        {
            _sacrificePanel.SetActive(toggle);
            _noSacrificePanel.SetActive(!toggle);
        }

        private void OnSacrificeStoneButtonClicked()
        {
            SacrificeRequest request = new SacrificeRequest
            {
                _type = MonoBehaviours.ResourceType.STONE
            };
            if (!_altarEntity.EntityManager.HasComponent<SacrificeRequest>(_altarEntity.Entity))
            {
                _altarEntity.EntityManager.AddComponentData(_altarEntity.Entity, request);
            }
        }
        
        private void OnSacrificeWoodButtonClicked()
        {
            SacrificeRequest request = new SacrificeRequest
            {
                _type = MonoBehaviours.ResourceType.WOOD
            };
            if (!_altarEntity.EntityManager.HasComponent<SacrificeRequest>(_altarEntity.Entity))
            {
                _altarEntity.EntityManager.AddComponentData(_altarEntity.Entity, request);
            }
        }
        
        private void OnSacrificeFoodButtonClicked()
        {
            SacrificeRequest request = new SacrificeRequest
            {
                _type = MonoBehaviours.ResourceType.FOOD
            };
            if (!_altarEntity.EntityManager.HasComponent<SacrificeRequest>(_altarEntity.Entity))
            {
                _altarEntity.EntityManager.AddComponentData(_altarEntity.Entity, request);
            }
        }
    }
}