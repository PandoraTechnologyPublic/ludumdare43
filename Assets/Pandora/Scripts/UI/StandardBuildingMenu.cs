﻿using Pandora.Data;
using Pandora.MonoBehaviours;
using TMPro;
using Unity.Entities;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora.UI
{
    public class StandardBuildingMenu : MonoBehaviour
    {
        [SerializeField] private Building _building;
        [SerializeField] private GameObjectEntity _entity;
    
        [SerializeField] private TMP_Text _nameText;
        [SerializeField] private TMP_Text _descriptionText;

        [SerializeField] private Button _destroyButton;

        private void Awake()
        {
            if (_destroyButton != null)
            {
                _destroyButton.onClick.AddListener(OnDestroyButtonPressed);
            }
        }
    
        private void Start()
        {
            if (_destroyButton != null)
            {
                _destroyButton.interactable = _building.isDestroyable;
            }

            if (_nameText != null)
            {
                _nameText.SetText(_building.buildingName);
            }

            if (_descriptionText != null)
            {
                _descriptionText.SetText(_building.buildingDescription);
            }
        }

        private void OnDestroyButtonPressed()
        {
            if (_entity != null && !_entity.EntityManager.HasComponent<ToDestroy>(_entity.Entity))
            {
                _entity.EntityManager.AddComponentData(_entity.Entity, new ToDestroy());
            }
        }
    }
}
