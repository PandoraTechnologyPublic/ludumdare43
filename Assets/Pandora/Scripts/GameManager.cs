﻿using System.Collections.Generic;
using Pandora.Data;
using Unity.Entities;
using UnityEngine;
using Building = Pandora.MonoBehaviours.Building;

namespace Pandora
{
    public class GameManager : MonoBehaviour
    {
        public long _initialGameTime = 20;
        public Material _allowedHologramMaterial;
        public Material _notAllowedHologramMaterial;
        [SerializeField] private List<Building> _buildingPrefabs;
        public GameObject _multipleSelectionPrefab;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void InitialiseBeforeSceneLoad()
        {
        
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public static void InitialiseAfterSceneLoad()
        {
            EntityManager entityManager = World.Active.GetOrCreateManager<EntityManager>();

            EntityArchetype gameStateEntityArchetype = entityManager.CreateArchetype(typeof(GameState), typeof(WelcomeState), typeof(ResourceState));
            Entity gameStateEntity = entityManager.CreateEntity(gameStateEntityArchetype);
            
            //entityManager.AddComponentData(gameStateEntity, new GameState());
            //entityManager.SetComponentData(gameStateEntity, new ResourceState {_foodQuantityCurrent = 50, _stoneQuantityCurrent = 200, _woodQuantityCurrent = 200});
        }

        public Building GetBuildingPrefabById(int buildingId)
        {
            Building selectedBuildingPrefab = null;
            
            foreach (Building buildingPrefab in _buildingPrefabs)
            {
                if (buildingPrefab.buildingId == buildingId)
                {
                    selectedBuildingPrefab = buildingPrefab;
                }
            }

            return selectedBuildingPrefab;
        }
    }
}