﻿using UnityEngine;
using UnityEngine.Events;

namespace Pandora
{
    public class PeriodicTrigger : MonoBehaviour
    {
        [SerializeField] private float interval;
        [SerializeField] private UnityEvent onPeriodicTrigger;

        private float _timeRemaining;

        private void Start()
        {
            _timeRemaining = interval;
        }

        private void Update()
        {
            _timeRemaining -= Time.deltaTime;

            if (_timeRemaining < 0)
            {
                onPeriodicTrigger.Invoke();
                _timeRemaining = interval;
            }
        }
    }
}
