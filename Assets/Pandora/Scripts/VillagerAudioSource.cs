﻿using Pandora.MonoBehaviours;
using UnityEngine;

public class VillagerAudioSource : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private AudioClip[] _hitWood;
    [SerializeField] private AudioClip[] _hitRock;
    [SerializeField] private AudioClip[] _hitFood;

    [SerializeField] private AudioClip _stockWood;
    [SerializeField] private AudioClip _stockFood;
    [SerializeField] private AudioClip _stockRock;

    public void PlayResourceHit(ResourceType resourceType)
    {
        switch (resourceType)
        {
            case ResourceType.FOOD:
            {
                playRandom(_hitFood);
                break;
            }

            case ResourceType.STONE:
            {
                playRandom(_hitRock);
                break;
            }

            case ResourceType.WOOD:
            {
                playRandom(_hitWood);
                break;
            }
        }

        _audioSource.Play();
    }

    public void playResourceStock(ResourceType resourceType)
    {
        switch (resourceType)
        {
            case ResourceType.FOOD:
            {
                _audioSource.clip = _stockFood;
                break;
            }

            case ResourceType.STONE:
            {
                _audioSource.clip = _stockRock;
                break;
            }

            case ResourceType.WOOD:
            {
                _audioSource.clip = _stockWood;
                break;
            }
        }

        _audioSource.Play();
    }

    public void playRandom(AudioClip[] clips)
    {
        if (clips.Length == 0)
        {
            return;
        }

        _audioSource.clip = clips[Random.Range(0, clips.Length)];
        _audioSource.Play();
    }
}
