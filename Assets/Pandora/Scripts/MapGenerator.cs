﻿using UnityEngine;

public class MapGenerator : MonoBehaviour
{
    [SerializeField] private GameObject bushPrefab;

    [SerializeField] private GameObject treePrefab;

    [SerializeField] private GameObject rockPrefab;

    [SerializeField] private Vector2 mapRangeX;

    [SerializeField] private Vector2 mapRangeZ;

    [SerializeField] private float minDistanceToOrigin;

    [SerializeField] private int numberOfRocks;

    [SerializeField] private int numberOfSingleBushes;

    [SerializeField] private int numberOfBushGroups;

    [SerializeField] private Vector2 bushesPerGroup;

    [SerializeField] private Vector2 bushesDispersion;

    [SerializeField] private int numberOfSingleTrees;

    [SerializeField] private int numberOfForests;

    [SerializeField] private Vector2 treesPerForest;

    [SerializeField] private Vector2 forestXDispersion;

    [SerializeField] private Vector2 forestZDispersion;

    public void GenerateMap()
    {
        SpawnSingles(bushPrefab, numberOfSingleBushes, mapRangeX, mapRangeZ);
        SpawnSingles(rockPrefab, numberOfRocks, mapRangeX, mapRangeZ);
        SpawnSingles(treePrefab, numberOfSingleTrees, mapRangeX, mapRangeZ);

        SpawnGroups(bushPrefab, numberOfBushGroups, bushesPerGroup, bushesDispersion, bushesDispersion);
        SpawnGroups(treePrefab, numberOfForests, treesPerForest, forestXDispersion, forestZDispersion);
    }

    private void SpawnSingles(GameObject prefab, int amount, Vector2 xRange, Vector2 zRange)
    {
        for (int i = 0; i < amount; i++)
        {
            float x = Random.Range(xRange.x, xRange.y);
            float z = Random.Range(zRange.x, zRange.y);

            Vector3 position = new Vector3(x, 0, z);
            Quaternion rotation = Quaternion.AngleAxis(Random.Range(0, 380), Vector3.up);

            // Don't put objects out of map
            if (position.x < mapRangeX.x || position.x > mapRangeX.y || position.z < mapRangeZ.x || position.z > mapRangeZ.y)
            {
                continue;
            }

            // Don't put objects too close to starting buildings
            if (Vector3.Distance(position, new Vector3(0,0,0)) < minDistanceToOrigin)
            {
                continue;
            }

            Instantiate(prefab, position, rotation);
        }
    }

    private void SpawnGroups(GameObject prefab, int numberOfGroups, Vector2 objectsPerGroup, Vector2 xDispersion, Vector2 zDispersion)
    {
        for (int group = 0; group < numberOfGroups; group++)
        {
            float xCenter = Random.Range(mapRangeX.x, mapRangeX.y);
            float zCenter = Random.Range(mapRangeZ.x, mapRangeZ.y);

            int numberOfObjects = (int) Random.Range(objectsPerGroup.x, objectsPerGroup.y);
            SpawnSingles(prefab, numberOfObjects, xDispersion + (Vector2.one * xCenter), zDispersion + (Vector2.one * zCenter));
        }
    }
}
