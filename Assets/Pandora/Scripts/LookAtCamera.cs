﻿using UnityEngine;

namespace Pandora
{
    public class LookAtCamera : MonoBehaviour
    {
        private void Update()
        {
            if (Camera.current != null)
            {
                transform.LookAt(Camera.current.transform);
                transform.Rotate(0, 180, 0);
            }
        }
    }
}
