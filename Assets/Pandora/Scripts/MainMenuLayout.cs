using Pandora.Data;
using Unity.Entities;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora
{
    public class MainMenuLayout : MonoBehaviour
    {
        [SerializeField] private Button _playButton;
        [SerializeField] private Button _optionsButton;
        [SerializeField] private Button _exitButton;

        private void Awake()
        {
            _playButton.onClick.AddListener(OnPlayButton);
            _optionsButton.onClick.AddListener(OnOptionsButton);
            _exitButton.onClick.AddListener(OnExitButton);
        }

        private void OnPlayButton()
        {
            GameObjectEntity gameObjectEntity = GetComponent<GameObjectEntity>();

            if (!gameObjectEntity.EntityManager.HasComponent<OnPlayButtonClicked>(gameObjectEntity.Entity))
            {
                gameObjectEntity.EntityManager.AddComponentData(gameObjectEntity.Entity, new OnPlayButtonClicked());
            }
        }

        private void OnOptionsButton()
        {
            
        }

        private void OnExitButton()
        {
            
        }
    }
}
