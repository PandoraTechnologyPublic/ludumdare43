using UnityEngine;
using UnityEngine.UI;

namespace Pandora
{
    public class ProgressBar : MonoBehaviour
    {
        [SerializeField] private Image _progressBarImage;

        public void SetProgress(float parProgress)
        {
            _progressBarImage.fillAmount = parProgress;
        }
    }
}
