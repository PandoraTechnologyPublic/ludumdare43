using Pandora.Data;
using Unity.Entities;
using UnityEngine;

namespace Pandora.MonoBehaviours
{
    [RequireComponent(typeof(GameObjectEntity))]
    public class Building : MonoBehaviour
    {
        public int buildingId;
        public string buildingName;
        public string buildingDescription;
        public bool isDestroyable;
        public bool isResourceStorage;

        private void Start()
        {
            if (isResourceStorage)
            {
                GameObjectEntity entity = GetComponent<GameObjectEntity>();
                entity.EntityManager.AddComponentData(entity.Entity, new ResourceStorageBuilding());
            }
        }
    }
}
