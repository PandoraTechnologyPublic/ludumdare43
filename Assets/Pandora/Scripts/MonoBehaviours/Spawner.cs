﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora.Components
{
    public class Spawner : MonoBehaviour
    {
        public enum SpawnState
        {
            SPAWNING,
            CAP_REACHED,
            NOT_ENOUGH_FOOD
        }

        [SerializeField] private Image _progress;
        [SerializeField] private Sprite _progressBar;
        [SerializeField] private Sprite _capReached;
        [SerializeField] private Sprite _foodRequired;

        public float _triggerTime;

        [HideInInspector] public float _deltaTime;
        [HideInInspector] public SpawnState _state;

        private void Update()
        {
            switch (_state)
            {
                case SpawnState.SPAWNING:
                    _progress.type = Image.Type.Filled;
                    _progress.sprite = _progressBar;
                    _progress.fillAmount = _deltaTime / _triggerTime;
                    break;

                case SpawnState.CAP_REACHED:
                    _progress.type = Image.Type.Simple;
                    _progress.sprite = _capReached;
                    break;

                case SpawnState.NOT_ENOUGH_FOOD:
                    _progress.type = Image.Type.Simple;
                    _progress.sprite = _foodRequired;
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}