﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectableEntity : MonoBehaviour
{
    public GameObject _selectionMesh;
    public GameObject _selectionUIInterface;
}
