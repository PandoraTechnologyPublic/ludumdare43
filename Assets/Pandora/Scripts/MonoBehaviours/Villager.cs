﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Pandora.Components
{
    public class Villager : MonoBehaviour
    {
        public string _currentAnimation;
        public AudioClip[] _sounds;
    }
}