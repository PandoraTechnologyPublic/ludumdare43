﻿using Pandora.Ui.ButtonClicker;
using TMPro;
using UnityEngine;

public class IdleVillagerButton : ButtonEntityClicker
{
    [SerializeField] private CanvasGroup _canvasGroup;
    [SerializeField] private TMP_Text _amountText;

    private int _amount = 0;

    public void Update()
    {
        if (Input.GetKeyUp(KeyCode.Space))
        {
            OnClick();
        }
    }

    public void SetAmount(int amount)
    {
        if (_amount != amount)
        {
            _amount = amount;

            if (_amount <= 0)
            {
                _canvasGroup.alpha = 0;
                _canvasGroup.interactable = false;
            }
            else
            {
                _canvasGroup.alpha = 1;
                _canvasGroup.interactable = true;
                _amountText.SetText(amount.ToString("D"));
            }
        }
    }
}
