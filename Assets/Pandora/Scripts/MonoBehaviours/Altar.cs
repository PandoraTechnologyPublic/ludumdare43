﻿using Pandora;
using Pandora.Data;
using TMPro;
using Unity.Entities;
using UnityEngine;

public enum AltarQuestState
{
    BEGINNING,
    WAITING,
    IN_PROGRESS
}

public class Altar : MonoBehaviour
{
    public AudioSource music, altarAudioSource, alwaysThunder; //sorry pour les noms mais il est 2h du mat :)

    public AudioClip newSacrifice, sacrificeSuccess, sacrificeFailed;

    [HideInInspector] public float _time;
    [HideInInspector] public AltarQuestState _state;
    
    public float _questTime;
    public float _nextQuestTime;
    public float _firstQuestDelay;

    [Space]
    public int _questBudget = 0;
    public int _budgetIncrementationFactor = 1;
    public float _budgetIncrementationTimeDelay = 10.0f;
    
    [Space]
    [SerializeField] public AltarQuestDisplay _display;
    public TMP_Text textOnScreen;

    public void Start()
    {
        GameObjectEntity entity = GetComponent<GameObjectEntity>();
        
        if (entity != null)
        {
            entity.EntityManager.AddComponentData(entity.Entity, new AltarQuestGenerationState
            {
                _incrementationFactor = _budgetIncrementationFactor,
                _increaseTime = _budgetIncrementationTimeDelay,
                _budget = _questBudget,
                _deltaTime = 0.0f,
            });
        }
    }
}
