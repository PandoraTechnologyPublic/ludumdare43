﻿using Pandora.Components;
using TMPro;
using UnityEngine;

public class BuildingMenuTooltip : MonoBehaviour
{
    [SerializeField] private TMP_Text _tooltipText;

    public void Show(string buildingName, BuildingCost cost, string buildingDescription)
    {
        string costString = "";

        if (cost._woodCost > 0)
        {
            costString = cost._woodCost + " Wood, ";
        }

        if (cost._stoneCost > 0)
        {
            costString += cost._stoneCost + " Stone";
        }
        else if (cost._woodCost > 0)
        {
            costString = costString.Substring(0, costString.Length - 2);
        }

        if (costString == "")
        {
            costString = "FREE";
        }

        _tooltipText.SetText("<b>" + buildingName + "</b> (" + costString + ")\n" + buildingDescription);
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
