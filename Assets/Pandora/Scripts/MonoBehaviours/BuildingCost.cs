﻿using UnityEngine;

namespace Pandora.Components
{    
    public class BuildingCost : MonoBehaviour
    {
        [SerializeField] public int _woodCost;
        [SerializeField] public int _stoneCost;
    }
}