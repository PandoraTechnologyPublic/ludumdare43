﻿using Pandora.Data;
using Unity.Entities;
using UnityEngine;

namespace Pandora.MonoBehaviours
{
    public enum ResourceType
    {
        WOOD,
        STONE,
        FOOD
    }
    
    public class ResourceStorage : MonoBehaviour
    {
        public int _maxValue;
        public ResourceType _type;
        public float _deltaTime;
        public int _storedValue;
        public bool _isResourceSource;

        private void Start()
        {
            if (_isResourceSource)
            {
                GameObjectEntity entity = GetComponent<GameObjectEntity>();
                if (entity != null)
                {
                    entity.EntityManager.AddComponentData(entity.Entity, new ResourceSource());
                }
            }
        }
    }
}