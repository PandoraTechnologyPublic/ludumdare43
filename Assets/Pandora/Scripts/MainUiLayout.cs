using TMPro;
using UnityEngine;

namespace Pandora
{
    public class MainUiLayout : MonoBehaviour
    {
        [SerializeField] private RectTransform _welcomeScreen;
        [SerializeField] private RectTransform _mainMenuScreen;
        [SerializeField] private RectTransform _ingameScreen;
        [SerializeField] private RectTransform _gameOverScreen;
        [SerializeField] private RectTransform _pauseMenuScreen;
        
        [SerializeField] private ProgressBar _evilSatisfactionProgressBar;

        public void SelectWelcomeScreen()
        {
            _welcomeScreen.gameObject.SetActive(true);
            _mainMenuScreen.gameObject.SetActive(false);
            _ingameScreen.gameObject.SetActive(false);
            _gameOverScreen.gameObject.SetActive(false);
            _pauseMenuScreen.gameObject.SetActive(false);
        }
        
        public void SelectMainMenuScreen()
        {
            _welcomeScreen.gameObject.SetActive(false);
            _mainMenuScreen.gameObject.SetActive(true);
            _ingameScreen.gameObject.SetActive(false);
            _gameOverScreen.gameObject.SetActive(false);
            _pauseMenuScreen.gameObject.SetActive(false);
        }
        
        public void SelectIngameScreen()
        {
            _welcomeScreen.gameObject.SetActive(false);
            _mainMenuScreen.gameObject.SetActive(false);
            _ingameScreen.gameObject.SetActive(true);
            _gameOverScreen.gameObject.SetActive(false);
            _pauseMenuScreen.gameObject.SetActive(false);
        }
        
        public void SelectGameOverScreen()
        {
            _welcomeScreen.gameObject.SetActive(false);
            _mainMenuScreen.gameObject.SetActive(false);
            _ingameScreen.gameObject.SetActive(false);
            _gameOverScreen.gameObject.SetActive(true);
            _pauseMenuScreen.gameObject.SetActive(false);
        }
        
        public void SelectPauseMenuScreen()
        {
            _welcomeScreen.gameObject.SetActive(false);
            _mainMenuScreen.gameObject.SetActive(false);
            _ingameScreen.gameObject.SetActive(false);
            _gameOverScreen.gameObject.SetActive(false);
            _pauseMenuScreen.gameObject.SetActive(true);
        }

        public void SetEvilSatisfaction(float gameStateEvilSatisfaction)
        {
            _evilSatisfactionProgressBar.SetProgress(gameStateEvilSatisfaction);
        }
    }
}
