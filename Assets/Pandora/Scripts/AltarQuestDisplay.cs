﻿using System;
using TMPro;
using UnityEngine;

namespace Pandora
{
    public class AltarQuestDisplay : MonoBehaviour
    {
   
        [SerializeField] public TMP_Text _TMPTime;
        [SerializeField] public TMP_Text _TMPVillager;
        [SerializeField] public TMP_Text _TMPStone;
        [SerializeField] public TMP_Text _TMPWood;
        [SerializeField] public TMP_Text _TMPFood;

        public void SetTime(float time)
        {
            _TMPTime.SetText(String.Format(
                "{0:D2}:{1:D2}",
                (int) Math.Floor(time / 60),
                (int) Math.Floor(time % 60)));
        }

        public void SetVillager(int count, int max)
        {
            _TMPVillager.SetText(String.Format("{0} | {1}", count, max));
        }
    
        public void SetStone(int count, int max)
        {
            _TMPStone.SetText(String.Format("{0} | {1}", count, max));
        }
    
        public void SetWood(int count, int max)
        {
            _TMPWood.SetText(String.Format("{0} | {1}", count, max));
        }
    
        public void SetFood(int count, int max)
        {
            _TMPFood.SetText(String.Format("{0} | {1}", count, max));
        }
    }
}
