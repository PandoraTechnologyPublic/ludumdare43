﻿using UnityEngine;

public class FreeLookCamera : MonoBehaviour
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private float zoomSpeed;
    [SerializeField] private float rotationSpeed;

    private Vector3 _rotation;

    private void Awake()
    {
        _rotation = transform.rotation.eulerAngles; 
    }

    private void Update()
    {
        /* Horizontal Translations */
        transform.position += transform.right * Input.GetAxisRaw("Horizontal") * moveSpeed;

        /* Vertical Translations */
        Vector3 front = transform.TransformDirection(Vector3.forward);
        front.y = 0;
        front.Normalize();
        transform.position += front * Input.GetAxisRaw("Vertical") * moveSpeed;

        /* Zoom */
        transform.position += transform.forward * Input.GetAxis("Mouse ScrollWheel") * zoomSpeed;

        /* Rotations */
        if (Input.GetMouseButtonDown(2))
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }

        if (Input.GetMouseButton(2))
        {
            //rotation.x -= Input.GetAxis("Mouse Y") * rotationSpeed;
            _rotation.y += Input.GetAxis("Mouse X") * rotationSpeed;
            _rotation.x = Mathf.Clamp(_rotation.x, 0, 45.0f);
            _rotation.y = Mathf.Repeat(_rotation.y, 360);
            transform.rotation = Quaternion.Euler(_rotation.x, _rotation.y, 0);
        }

        if (Input.GetMouseButtonUp(2))
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }
}