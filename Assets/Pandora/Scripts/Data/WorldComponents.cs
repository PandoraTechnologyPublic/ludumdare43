﻿using Unity.Entities;

namespace Pandora.Data
{
    public struct BuildingHologram : IComponentData
    {
        public int _isDirty;
        public int _buildingId;

        public BuildingHologram(int buildingId)
        {
            _isDirty = 0;
            _buildingId = buildingId;
        }
    }

    public struct CameraFocused : IComponentData { }
}
