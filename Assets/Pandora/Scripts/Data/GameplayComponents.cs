﻿using System.ComponentModel;
using Pandora.MonoBehaviours;
using Unity.Entities;
using UnityEngine;
using UnityEngine.UI;

namespace Pandora.Data
{
    public enum HarvestingState
    {
        HARVESTING,
        TRANSFERING,
        SEARCHING
    }

    public struct VillagerData : IComponentData
    {
        public int level;
        public int xp;
        public int nextLevelXp;
    }

    public struct SoldierData : IComponentData
    {
        public int level;
        public int xp;
        public int nextLevelXp;
    }

    public struct BuildingId : IComponentData
    {
        public int _id;

        public BuildingId(int id)
        {
            _id = id;
        }
    }

    public struct GameState : IComponentData
    {
        public float _evilSatisfaction;
        // percent of _evilSatisfaction drained per second
        public float _evilDrainRate;
    }

    public struct ResourceState : IComponentData
    {
        public int _populationQuantityMax;
        public int _populationQuantityCurrent;
        public int _foodQuantityMax;
        public int _foodQuantityCurrent;
        public int _woodQuantityMax;
        public int _woodQuantityCurrent;
        public int _stoneQuantityMax;
        public int _stoneQuantityCurrent;
    }

    public struct AltarQuestGenerationState : IComponentData
    {
        public int _budget;
        public float _increaseTime;
        public int _incrementationFactor;
        public float _deltaTime;
    }
    
    public struct ResourceStorageBuilding : IComponentData {}
    
    public struct ResourceSource : IComponentData {}
    
    public struct ResourceExhausted : IComponentData {}
        
    public struct BeingHarvested : IComponentData
    {
        public int _harvesterCount;
    }

    public struct Harvesting : IComponentData
    {
        public Entity _targetEntity;
        public HarvestingState _state;
        public Vector3 _position;
    }

    public struct Interacting : IComponentData
    {
        public Entity _targetEntity;
    }

    public struct BeingInteracted : IComponentData
    {
        public int _actorCount;
    }
    
    public struct AltarQuest : IComponentData
    {
        public int _villagerRequested;
        public int _foodRequested;
        public int _woodRequested;
        public int _stoneRequested;

        public int _currentVillagerCount;
        public int _currentFoodCount;
        public int _currentWoodCount;
        public int _currentStoneCount;
        
        public float _remainingTime;
        public int _displayed;
    }

    public struct SacrificeRequest : IComponentData
    {
        public ResourceType _type;
    }

    public struct Used : IComponentData
    {
    }
}
