﻿using Unity.Entities;

namespace Pandora.Data
{
    public struct Clicked : IComponentData
    {
        public int _frameClickedIndex;
    }
    public struct Selected : IComponentData { }
    public struct ToDestroy : IComponentData { }


    public struct WelcomeState : IComponentData {}
    public struct MainMenuState : IComponentData {}
    public struct EnterGameState : IComponentData {}
    public struct IngameState : IComponentData {}
    public struct ExitGameState : IComponentData {}
    public struct PauseMenuState : IComponentData {}
    public struct GameOverState : IComponentData {}
    
    public struct OnAnyKeyUp : IComponentData {}
    public struct OnEscapeKeyUp : IComponentData {}
    public struct OnPlayButtonClicked : IComponentData {}
}