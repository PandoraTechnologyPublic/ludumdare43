﻿using System.ComponentModel;
using Unity.Entities;
using UnityEngine;

namespace Pandora.Data
{
    public enum TargetType
    {
        MOVE,
        ACTION
    }
    
    public struct Target : IComponentData
    {
        public Vector3 _position;
        public TargetType _type;
    }
}