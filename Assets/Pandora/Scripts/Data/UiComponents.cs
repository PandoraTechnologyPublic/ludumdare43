﻿using Unity.Entities;

namespace Pandora.Data
{
    public struct Ui : IComponentData { }

    public struct SelectedBuilding : IComponentData
    {
        public int _isAllowed;
    }
}
